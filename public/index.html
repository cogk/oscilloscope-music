<!DOCTYPE html>
<html lang="en">

  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Oscilloscope Music interactive article</title>
    <link rel="stylesheet" href="style.css" />

    <script src="lib/Tone.min.js"></script>
    <script src="lib/math.min.js"></script>
    <script src="lib/woscope.js"></script>
  </head>

  <body>

    <article class="oscilloscope-music-article">

      <header class="om-two-cols">
        <div>
          <h1>Oscilloscope Music</h1>
          <p style="margin-bottom: 2em;">
            An interactive article by Corentin Forler &mdash;
            <time datetime="2022-07-14">
              14 July 2022
            </time>
          </p>

          <p>
            By using oscilloscopes, devices that can show electrical signals on a screen, musicians have been able to draw shapes with sounds.
          </p>
          <p>
            With this interactive article, you will be able to explore and understand this ingenious way of drawing shapes by studying a particular genre of music called <strong>oscilloscope music</strong>.
          </p>
        </div>

        <div class="om-flex-center">
          <img class="om-img" style="transform-origin: right top;" src="img/intro-flower.png" alt="" />
        </div>
      </header>

      <hr />

      <div class="om-two-cols">
        <div>
          <h2>Oscillators</h2>
          <p>
            In the physical realm, a sound is produced by the vibration of a <strong>source</strong>.
            Musical instruments are sources, they produce vibrations that can travel through the air before reaching a receiver such as a microphone or an ear-drum.
          </p>
          <p>
            A simple sound source is the <strong>tuning fork</strong>.
            When it is struck, it resonates at a specific frequency, which creates a sound.
          </p>
          <p>
            This vibration can also be called an oscillation, and the tuning fork is a simple <strong>oscillator</strong>.
          </p>
        </div>
        <div class="om-flex-center">
          <img class="om-img" src="img/explain-tuning-fork.jpg" alt="" />
        </div>
      </div>

      <div class="om-two-cols">
        <div class="om-flex-center" style="justify-content: space-around;">
          <p>
            Click on the audio below to play the sound of a tuning fork. You can also click on the picture of the tuning fork to play the sound.
          </p>
          <audio volume="0.4" src="sound/tuning-fork.mp3" controls id="om-tuning-fork-audio"></audio>
        </div>
        <div class="om-flex-center" id="om-tuning-fork">
          <img onclick="document.getElementById('om-tuning-fork-audio').play()" src="img/tuning-fork.jpg" alt="A picture of a tuning fork." />
        </div>
      </div>

      <br /><br />

      <h2>Sine waves</h2>

      The sound created by a tuning fork is a <strong>sine wave</strong>.
      Most of the time, tuning forks are used to tune musical instruments, and resonate at a frequency of 440 Hertz.

      <br /><br />
      <div id="om-section-1"></div>

      <br /><br />
      <h2>Synthesizers: the birth of new sounds</h2>
      <p>In the world of microphones and computers, a sound is an electrical signal moving through wires, just like it can be transmitted by the air to an ear.</p>
      <p>The transformation of sounds into electrical signals is what has enabled many new ways of creating, recording, transmitting and experiencing sounds: microphones, loudspeakers, telephones, radios, electric guitars, synthesizers, and computers.</p>

      <div class="om-two-cols">
        <div>
          <p>
            Different physical instruments can produce the same note but still sound different, because they have different <strong>timbres</strong>. With synthesizers, musicians were able to create completely new sounds, without the need of a physical source of vibrations.
          </p>
          <p>
            The timbre of a simple synthesizer can be any <strong>waveform</strong>, also called oscillators, or combination of waveforms. The most simple waveforms are:
          </p>
          <ul>
            <li>The <button class="om-try-it-link" onclick="section2_waveform_toggle('sine')">sine</button> wave</li>
            <li>The <button class="om-try-it-link" onclick="section2_waveform_toggle('square')">square</button> wave</li>
            <li>The <button class="om-try-it-link" onclick="section2_waveform_toggle('triangle')">triangle</button> wave</li>
            <li>The <button class="om-try-it-link" onclick="section2_waveform_toggle('sawtooth')">sawtooth</button> wave</li>
          </ul>
          <p>
            To hear them, you can use the selection menu called <em>Waveform</em> or simply click on them in the list above.
          </p>
        </div>
        <div>
          <div id="om-section-2"></div>
        </div>
      </div>

      <br />
      <p>
        While the a basic synthesizer may <em>sound</em> simple, it still can be used just like any other instruments to create music.
        Below is an example of a major chord being played using the simple waveforms introduced above. You can change the values as you wish to create new chords, just click on the triangle <b>▶︎</b> to open the menu of each oscillator.
      </p>
      <br />

      <div id="om-section-3"></div>

      <h2>Stereo audio and oscilloscopes</h2>

      <div class="om-two-cols">
        <div>
          <p>
            A sound or music is said to be <strong>stereo</strong> when it is made up of two distinct signals, also called <strong>channels</strong>, one for the left ear and one for the right ear. This is where we can start to understand the origins of oscilloscope music.
          </p>
          <p>
            Indeed, oscilloscopes are used to show electrical signals on a screen, and they can be controlled by two signals at the same time, one for the <strong>x</strong> axis and one for the <strong>y</strong> axis. Those two signals control the position of the dot on the screen, but because this dot moves really fast, it is seen as a single continunous line.
          </p>
        </div>
        <div>
          <img class="om-img" src="img/oscilloscope.svg" alt="" />
        </div>
      </div>

      <p class="om-pullout-quote">
        Oscilloscope music merges the <em>XY mode</em> of oscilloscopes with the world of stereo audio by controlling the <strong>x</strong> axis with the <strong>left</strong> audio channel, and the <strong>y</strong> axis with the <strong>right</strong> channel.
      </p>

      <div style="position: relative;">
        <p>
          Here is an example of a simple Lissajous figure being created using two audio signals. The left signal is a sine wave of a given frequency, and the right signal is a cosine wave (a sine wave shifted a little bit in time) of twice the frequency.
        </p>
        <p class="om-sticky-text">
          Notice how the shape changes depending on the frequency and volume of the two signals. Try frequencies where one is a multiple of the other, such as
          <button class="om-try-it-link" onclick="section4_lissajous_change(300, 200)">300 Hz and 200 Hz</button>,
          <button class="om-try-it-link" onclick="section4_lissajous_change(300, 400)">300 Hz/400 Hz</button>,
          <button class="om-try-it-link" onclick="section4_lissajous_change(800, 400)">800 Hz/400 Hz</button>,
          <button class="om-try-it-link" onclick="section4_lissajous_change(200, 300)">200 Hz/300 Hz</button>,
          or even when they are
          <button class="om-try-it-link" onclick="section4_lissajous_change(300, 300)">the same frequency</button>.
        </p>
        <div id="om-section-4"></div>
      </div>

      <br /><br />

      <div style="position: relative;">
        <h2>Experimenting with waves</h2>
        <p>
          Here you can experiment with sums of multiple waves.
        </p>
        <p class="om-sticky-text">
          Notice how the shape changes depending on the frequency, volume and phase of the signals.
          Try the following examples:
          <button class="om-try-it-link" onclick="section5_flower_change([[400, 0, 0.5], [100, 90, 0.5], [400, 90, 0.5], [100, 0, 0.5]])">a flower with 5 petals</button>, or
          <button class="om-try-it-link" onclick="section5_flower_change([[500, 0, 0.5], [100, 0, 0.5], [500, 90, 0.5], [100, 90, 0.5]])">4 petals and 4 sepals</button>, or
          <button class="om-try-it-link" onclick="section5_flower_change([[75, 90, 0.5], [300, 180, 0.5], [75, 0, 0.5], [300, 90, 0.5]])">3 petals</button>, or
          <button class="om-try-it-link" onclick="section5_flower_change([[400, 0, 0.5], [500, 90, 0.5], [400, 90, 0.5], [500, 0, 0.5]])">8 petals</button>, or even a
          <button class="om-try-it-link" onclick="section5_flower_change([[100, 0, 1, 'sawtooth'], [0, 0, 0], [0, 0, 0], [200, 0, 1]])">sine wave</button>, or a
          <button class="om-try-it-link" onclick="section5_flower_change([[100, 0, 1, 'sawtooth'], [0, 0, 0], [0, 0, 0], [200, 0, 0.3, 'sawtooth']])">sawtooth wave</button>, or a
          <button class="om-try-it-link" onclick="section5_flower_change([[100, 0, 1, 'sawtooth'], [0, 0, 0], [0, 0, 0], [200, 0, 0.3, 'square']])">square wave</button>.
        </p>
        <br /><br />
        <div id="om-section-5"></div>
        <!-- <div class="om-two-cols">
          <div>
            <div id="om-section-5"></div>
          </div>
          <div class="om-flex-center">
            <div id="om-section-5-oscilloscope"></div>
          </div>
        </div> -->
      </div>

      <br /><br />

      <!-- <button id="om-click-notice">
        <div>Click here to start the demo</div>
      </button> -->

      <br /><br />

      <br /><br />
      <div style="display:flex;flex-direction:column;justify-content:center;align-items:stretch;gap:8px" id="om-xy-playground">
        <h2>Oscilloscope music playground</h2>

        <div style="display: flex; flex-direction: row; flex-wrap: wrap; gap: 5px;">
          <label>
            Examples:
            <select id="om-xy-example-dropdown"></select>
          </label>
          <button style="display: block;" type="button" id="om-xy-example-next-button" class="om-try-it-link">Next example</button>
        </div>
        <br />

        <textarea id="om-xy-input" style="height: 200px;">x = 0.5 cosine(220 Hz)
y = 0.5 sine(440 Hz)</textarea>
        <!--
        F = 220
        F2 = 110
        F3 = 400 # 3F

        gap = 0.1
        CX = -0.6
        CY = -0.6
        CW = 0.3
        CH = 0.3

        w1 = 1 - CH - 2 gap
        h2 = 1 - CW - 2 gap

        f1 = F3
        x1 = ramp(f1) * w1 + (1 + CX)
        y1 = sin(2 f1) * t^2 CH + CY

        f2 = F3
        x2 = sin(2 f2) * t^2 CW + CX
        y2 = ramp(f2) * h2 + (1 + CY)

        f3 = F3
        x3 = cos(f3) * t^2 CW + CX
        y3 = sin(f3) * t^2 CH + CY

        f4 = F3
        x4 = cos(f4) * (noise(1)^.5) * t CW + (1 + CX)
        y4 = sin(f4) * (noise(2)^.5) * t CH + (1 + CY)

        # Output:
        x = share(F2, x1, x2, x3, x4)
        y = share(F2, y1, y2, y3, y4)
       -->

        <button id="om-xy-play">
          <apply-template template="om-play-button"></apply-template>
        </button>

        <div style="display:flex;flex-direction:row;justify-content:center;align-items:center;flex-grow:0;">
          <canvas id="om-xy-white-canvas" width="500" height="500" style="flex:1;max-width:50%;"></canvas>
          <canvas id="om-xy-black-canvas" width="500" height="500" style="flex:1;width:50%;max-width: 512px;background:black;"></canvas>
        </div>

        <div style="display: flex; flex-direction: row; justify-content: space-between; align-items: center;">
          <!-- <input id="om-freqMult-slider" type="range" min=0.25 max=2 step=0.25 value=1 /> -->
          <label>
            Volume:
            <input id="om-volume-slider" type="range" min=0 max=1 step=0.01 value=0.3 />
          </label>

          <label>
            Duration:
            <input id="om-render-duration" type="number" min=0.5 max=20 step=0.5 value=1 />
          </label>
        </div>

      </div>

      <div style="height: 10vh;"></div>

      <footer>
        <h2>Sources</h2>

        <h3>Audio/Image Sources</h3>

        <ul>
          <li>
            Picture.
            <a href="https://commons.wikimedia.org/wiki/File:PSM_V13_D055_Tuning_fork_and_sound_vibration.jpg"><q>Tuning fork and sound vibration</q></a>,
            public domain
          </li>
          <li>
            Picture.
            <a href="https://commons.wikimedia.org/wiki/File:Tuning-fork.jpg"><q>Tuning Fork</q></a> by Helihark,
            licensed under <a href="https://creativecommons.org/licenses/by-sa/3.0/deed.en">CC-BY-SA 3.0 Unported License</a>
          </li>
          <li>
            Audio.
            <a href="https://freesound.org/people/jmuehlhans/sounds/220747/"><q>Tuning Fork 440 Hz, Resonance Box</q> by jmuehlhans</a>,
            licensed under <a href="https://creativecommons.org/publicdomain/zero/1.0/">CC0 1.0 Universal (Public Domain)</a>
          </li>
        </ul>

        <h3>References</h3>

        <ul>
          <li>
            Video.
            <a href="https://www.youtube.com/watch?v=rtR63-ecUNo">
              <q>How To Draw Mushrooms On An Oscilloscope With Sound</q>
              by Jerobeam Fenderson.
            </a>
            <time datetime="2014-03-26">26 March 2014</time>
          </li>
          <li>
            Book.
            <q>Principles of musical acoustics</q> by William M Hartmann
            <time datetime="2014-03">26 March 2014</time>
          </li>
        </ul>

        <h3>JavaScript Libraries</h3>

        <ul>
          <li><a href="https://tonejs.github.io/">Tone.js</a></li>
          <li><a href="https://github.com/m1el/woscope">woscope</a></li>
          <li><a href="https://mathjs.org/">math.js</a></li>
        </ul>
      </footer>
      <div style="height: 10vh;"></div>
    </article>














    <template id="om-play-button">
      <svg role="button" aria-label="Play" class="om-play-button" viewBox="0 0 100 100" width="48" height="48" style="color:#2a2;">
        <ellipse fill="currentColor" cx="50" cy="50" rx="45" ry="45" />
        <polygon fill="rgba(255,255,255,.3)" stroke="rgba(255,255,255,.6)" stroke-width="4" points="35,30 35,70 75,50" />
      </svg>
    </template>

    <template id="om-oscilloscope">
      <canvas mode="trail" class="om-oscilloscope" width=400 height=400></canvas>
    </template>

    <template id="om-osc-details-summary">
      <span class="om-osc-details-summary">
        <span slot-parent="type"><span slot-key="fmt(type)"></span> wave</span>
        <span slot-parent="freq"><span slot-key="freq"></span> Hz</span>
        <span slot-parent="phase"><span slot-key="phase"></span>°</span>
        <span slot-parent="channel"><span slot-key="fmt(channel)"></span></span>
      </span>
    </template>

    <template id="om-osc-details-content">
      <div class="om-osc-details">
        <label class="om-osc-details-val" slot-parent="type">
          <span class="om-osc-details-val-label">Waveform:</span>
          <select slot-key="type">
            <option value="sine">Sine</option>
            <option value="triangle">Triangle</option>
            <option value="sawtooth">Sawtooth</option>
            <option value="square">Square</option>
          </select>
        </label>
        <label class="om-osc-details-val" slot-parent="channel">
          <span class="om-osc-details-val-label">Position:</span>
          <select slot-key="channel">
            <option value="C">Center</option>
            <option value="L">Left (X)</option>
            <option value="R">Right (Y)</option>
          </select>
        </label>
        <div class="om-osc-details-val" slot-parent="freq">
          f =
          <input type="range" min=25 max=1000 step=25 slot-key="freq" />
          <input type="number" min=10 max=1000 step=1 slot-key="freq" />
          <!-- <span slot-key="freq"></span> -->
          Hz
        </div>
        <div class="om-osc-details-val" slot-parent="phase">
          φ =
          <input type="range" min=0 max=360 step=5 slot-key="phase" />
          <input type="number" min=0 max=360 step=5 slot-key="phase" />
          <!-- <span slot-key="phase"></span> -->
          °
        </div>
        <div class="om-osc-details-val" slot-parent="gain">
          Volume:
          <input type="range" min=0 max=1 step=0.01 slot-key="gain" />
        </div>
      </div>
    </template>

    <template id="om-osc-details">
      <div class="om-osc-details-container">
        <details class="om-osc-details">
          <summary>
            <b class="om-click-to-close">Click to close settings</b>
            <apply-template template="om-osc-details-summary"></apply-template>
          </summary>
          <apply-template template="om-osc-details-content"></apply-template>
        </details>
      </div>
    </template>

    <template id="om-osc">
      <div class="om-osc">
        <apply-template template="om-osc-details"></apply-template>
        <canvas height="150" width="400"></canvas>
        <div class="om-osc-desc">
          <div slot-key="desc"></div>
        </div>
      </div>
    </template>

    <template id="om-osc-group">
      <div class="om-osc-group">
        <div class="om-osc-group--oscillators" slot-key="oscillators" class="om-flex-row"></div>
      </div>
    </template>

    <template id="om-section">
      <div class="om-section">
        <div class="om-section--groups" slot-key="groups"></div>
        <div class="om-section--desc">
          <div slot-key="desc"></div>
        </div>
        <div class="om-section--side-controls">
          <apply-template template="om-play-button"></apply-template>
        </div>
        <div class="om-section--bottom-controls">

        </div>
        <div class="om-section--oscilloscope">
          <div style="font-size:initial;padding-bottom:4px;">
            <b>Oscilloscope:</b>
          </div>
          <apply-template template="om-oscilloscope"></apply-template>
        </div>
      </div>
    </template>

    <script>
      function section2_waveform_toggle(type) {
        const el = document.querySelector(`#om-section-2 .om-osc select[slot-key='type']`)
        el.value = type
        el.dispatchEvent(new Event('input'))
      }
      function section4_lissajous_change(f1, f2) {
        const spec = om_sections['om-section-4']
        if (spec && spec.rerender) {
          spec.oscGroups[0].oscillators[0].freq = f1
          spec.oscGroups[1].oscillators[0].freq = f2
          spec.rerender()
          if (spec.playSound) {
            spec.playSound()
          }
        }
      }
      function section5_flower_change(overrides) {
        const spec = om_sections['om-section-5']
        if (spec && spec.rerender) {
          let i = 0
          for (const o of overrides) {
            const [f, ph, gain, type = 'sine'] = o
            spec.oscGroups[0].oscillators[i].freq = f
            spec.oscGroups[0].oscillators[i].phase = ph
            spec.oscGroups[0].oscillators[i].gain = gain
            spec.oscGroups[0].oscillators[i].type = type
            i++
          }
          spec.rerender()
          if (spec.playSound) {
            spec.playSound()
          }
        }
      }

      document.getElementById('om-tuning-fork-audio').volume = 0.3
    </script>
    <script src="script.js"></script>
  </body>

</html>
