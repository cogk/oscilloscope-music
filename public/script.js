const { floor, sin, cos, abs, sign, round, PI } = Math
const req = (x = '') => { throw new TypeError('required parameter', x) }
const assert = (x) => {
}

const bifilter = (arr, fn) => {
  const match = []
  const noMatch = []
  arr.forEach(el => {
    if (fn(el)) {
      match.push(el)
    } else {
      noMatch.push(el)
    }
  })
  return [match, noMatch]
}


const OMColors = {
  red: '#b71c1c',
  pink: '#880e4f',
  purple: '#4a148c',
  deepPurple: '#8000cc',
  indigo: '#1a237e',
  blue: '#0d47a1',
  lightBlue: '#01579b',
  cyan: '#006064',
  teal: '#004d40',
  green: '#1b5e20',
  limeGreen: '#33691e',
  lime: '#827717',
  yellow: '#f57f17',
  amber: '#ff6f00',
  orange: '#e65100',
  deepOrange: '#bf360c',
  brown: '#3e2723',
  grey: '#212121',
  blueGrey: '#263238',
}


class OMApp {
  constructor() {
    this.freqMult = 1
    this.shownPeriods = 3
    this.synths = {}
    this.started = false

    this._onStart = []
  }
  get baseFreq() {
    return this.freqMult * 400
  }
  setup() {
    this.main = new Tone.Gain(3)

    this.lowpass = new Tone.Filter(20000, "lowpass")
    this.highpass = new Tone.Filter(10, "highpass")
    this.compressor = new Tone.Compressor(-15, 12)
    this.master = new Tone.Gain(1)

    Tone.getDestination().chain(
      this.main,
      this.lowpass,
      this.highpass,
      this.compressor,
      this.master,
    )

    this.L = new Tone.Panner(-1).connect(this.main)
    this.R = new Tone.Panner(+1).connect(this.main)
    this.C = new Tone.Panner(0).connect(this.main)
    // this.synth = new Tone.Synth().connect(this.C)
  }

  async start() {
    if (this.started) return
    this.started = true
    // this.context = new Tone.Context({ latencyHint: 'playback' })
    this.context = new Tone.Context({ latencyHint: 'interactive' })
    Tone.setContext(this.context)
    await Tone.start()
    this.setup()

    document.getElementById('om-click-notice')?.remove()
    this._onStart.forEach(cb => cb(this))
  }

  waitForStart(cb) {
    this._onStart.push(cb)
  }

  destroyWoscope(canvas) {
    if (this.woscope) {
      // https://github.com/m1el/woscope/blob/master/dist/demo.js
      this.woscope.sourceNode = null
      this.woscope.destroy()

      // replace canvas. more compatible than restoring gl context on old canvas
      const copy = canvas.cloneNode(true)
      canvas.parentNode.replaceChild(copy, canvas)
      canvas = copy
      this.woscope = null
    }
    return canvas
  }

  initWoscope(canvas = req(), nativeSource = null) {
    canvas = this.destroyWoscope(canvas)
    canvasFixDpi(canvas)
    const ac = window.AudioContext
    const sourceNode = nativeSource || this.main._gainNode._nativeAudioNode
    const actx = Tone.getContext()._context._nativeAudioContext
    // dependency injection
    window.AudioContext = function() { return actx }

    this.woscope = woscope({
      canvas: canvas,
      sourceNode: sourceNode,
      live: true,
      bloom: true,
      audioUrl: 'void:',
      //callback: function () { console.log('woscope okay') },
      error: function (msg) { console.error('woscope error:', msg) },
    })
    window.AudioContext = ac

    sourceNode.disconnect(actx.destination)
    return canvas
  }

  initWoscopeWithAudioElem(canvas = req('canvas'), audio = req('audio')) {
    canvas = this.destroyWoscope(canvas)
    canvasFixDpi(canvas)
    const ac = window.AudioContext
    const actx = Tone.getContext()._context._nativeAudioContext
    // dependency injection
    window.AudioContext = function() { return actx }

    this.woscope = woscope({
      canvas: canvas,
      audio: audio,
      //callback: function () { console.log('woscope okay') },
      error: function (msg) { console.error('woscope error:', msg) },
    })
    window.AudioContext = ac
    return canvas
  }
}

const app = new OMApp()

function getFuncForSignal(type=req(), freq=req(), phase=0) {
  let T = freq / app.baseFreq
  if (T > 0.1 && T < 1) { T = Math.ceil(T) }

  const phase01 = phase / 180
  const phaseRad = PI * phase01
  if (type === 'triangle') {
    return (ot) => {
      const t = ot * T + phase01
      return 4 * Math.abs(t - Math.floor(t + 1/2)) - 1
    }
  } else if (type === 'sine') {
    return (t) => Math.sin(t * T * 2*PI + phaseRad)
  } else if (type === 'square') {
    return (t) => (Math.sign(Math.sin(t * T * 2*PI + phaseRad)) || 1)
  } else if (type === 'sawtooth') {
    return (ot) => {
      const t = ot * T + phase01
      return 2 * (t - Math.floor(t + 1/2))
    }
  }
}

function convertRect(rect, ctx = req(), margin=0) {
  const [W, H] = [ctx.canvas.width, ctx.canvas.height]
  const { x, y, w, h } = rect
  const s = (v, V) => {
    if (typeof v === 'function') return v(V, {H,W})
    else if (v > 0 || Object.is(v, 0)) return v
    else if (v < 0 || Object.is(v, -0)) return (V + v)
  }
  return {
    x: s(x, W - 2 * margin) + margin,
    y: s(y, H - 2 * margin) + margin,
    w: s(w, W - 2 * margin),
    h: s(h, H - 2 * margin),
    X: margin,
    Y: margin,
    W: W - 2 * margin,
    H: H - 2 * margin,
  }
}

function drawOscillator(osc, ctx, color = 'black', gain = 1, rect = {x:0,y:0,w:-0,h:-0}) {
  const freq = Number(osc.frequency.value)
  const type = String(osc.baseType)
  const phase = Number(osc.phase)
  const func = getFuncForSignal(type, freq * app.shownPeriods, phase)
  const draw = drawOscFunc(func)

  const dpr = canvasFixDpi(ctx)
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
  drawGrids(ctx, convertRect({x:0,y:0,w:-0,h:-0}, ctx), [
    {
      nRows: 2,
      nCols: 0,
      color: 'rgba(0,0,0,0.25)',
      width: 1,
      excludeBorders: 1,
    },
  ])
  draw(osc, freq, ctx, rect, color, typeof gain === 'number' ? gain : 1)
}

function drawOscFunc(func){ return function (osc, freq, ctx, rect, color, gain) {
  const dpr = window.devicePixelRatio || 1
  const lineWidth = 5 * dpr
  const { x, y, w, h, X, Y, W, H } = convertRect(rect, ctx, lineWidth)
  const [mx, my] = [x + w/2, y + h/2]

  const f = (t) => func(t / w)

  ctx.beginPath()
  ctx.strokeStyle = color
  ctx.lineWidth = lineWidth
  ctx.lineCap = 'round'
  ctx.lineJoin = 'round'
  const s = (t) => [x + t, -gain * f(t) * ((h - y)/2) + my]
  ctx.moveTo(...s(0))
  for (let t = 0; t <= w; t += 4) {
    ctx.lineTo(...s(t))
  }
  ctx.stroke()
}}

function iterateSlot(element, key, f) {
  const inputs = element.querySelectorAll(`:is(input,select)[slot-key="${key}"]`)
  inputs.forEach(el => f(el, 'rw'))

  const els = element.querySelectorAll(`slot[key="${key}"],:not(input,select)[slot-key="${key}"]`)
  els.forEach(el => f(el, 'ro'))
}

function setSlot(element, key, value) {
  iterateSlot(element, key, (el, rw) => {
    if (rw === 'rw') {
      el.value = String(value)
    } else if (rw === 'ro') {
      if (value instanceof Node) {
        el.appendChild(value)
      } else if (typeof value.html === 'string') {
        el.innerHTML = value.html
      } else {
        el.innerText = String(value)
      }
    }
  })
}

function hideSlotParent(element, key) {
  const els = element.querySelectorAll(`[slot-parent="${key}"]`)
  els.forEach(el => el.setAttribute('hidden', 'hidden'))
}
function showSlotParent(element, key) {
  const els = element.querySelectorAll(`[slot-parent="${key}"]`)
  els.forEach(el => el.removeAttribute('hidden'))
}

function formatOscType(t, phase) {
  if (t === 'sine' && phase === 90) {
    return 'Cosine'
  }
  switch (t) {
    case 'sine': return 'Sine'
    case 'square': return 'Square'
    case 'sawtooth': return 'Sawtooth'
    case 'triangle': return 'Triangle'
    default: return t
  }
}
function formatChannel(t) {
  switch (t) {
    case 'L': return 'Left'
    case 'R': return 'Right'
    default: return 'Center'
  }
}

function canvasFixDpi(ctx) { // https://web.dev/canvas-hidipi/
  const canvas = (ctx instanceof Node) ? ctx : ctx.canvas
  // if (canvas.dprFixed) { return }

  // Get the device pixel ratio, falling back to 1.
  const dpr = window.devicePixelRatio || 1
  if (dpr === 1) return 1
  // Get the size of the canvas in CSS pixels.
  const rect = canvas.getBoundingClientRect()
  // Give the canvas pixel dimensions of their CSS
  // size * the device pixel ratio.
  const w = Math.round(rect.width * dpr)
  const h = Math.round(rect.height * dpr)
  if (canvas.width !== w && canvas.height !== h) {
    canvas.width = w
    canvas.height = h
    canvas.dprFixed = true
  }
  // Scale all drawing operations by the dpr, so you
  // don't have to worry about the difference.
  //ctx.setTransform(dpr, 0, 0, dpr, 0, 0)
  return dpr
}

function getTemplate(id, slots = {}) {
  const template = document.getElementById(id)
  const clone = document.importNode(template.content, true).firstElementChild
  for (const k in slots) {
    const v = slots[k]
    setSlot(clone, k, v)
  }

  for (const el of clone.querySelectorAll('apply-template')) {
    const id = el.getAttribute('template')
    const childSlots = Object.fromEntries(
      el.getAttributeNames()
        .map(k => [k, el.getAttribute(k)])
    )
    for (const k in slots) {
      if (childSlots[k] === undefined) {
        childSlots[k] = slots[k]
      }
    }
    el.replaceWith(getTemplate(id, childSlots))
  }
  return clone
}


function drawGrids(ctx, { X, Y, W, H }, grids) {
  const dpr = window.devicePixelRatio
  const line = (x,y,a,b) => { ctx.moveTo(x, y); ctx.lineTo(a, b) }

  for (const grid of grids) {
    ctx.lineWidth = grid.width * dpr
    ctx.strokeStyle = grid.color
    ctx.beginPath()
    const gridSize = grid.nDivs || -1
    const nCols = grid.nCols || gridSize
    const nRows = grid.nRows || gridSize

    const excludeBorders = grid.excludeBorders ? 1 : 0
    for (let i = excludeBorders; i <= nCols - excludeBorders; i++) {
      const x = ctx.lineWidth/2 + X + (W - ctx.lineWidth) * (i/nCols)
      const y1 = Y
      const y2 = Y + H
      line(x, y1, x, y2)
    }
    for (let j = excludeBorders; j <= nRows - excludeBorders; j++) {
      // const j = i
      const y = ctx.lineWidth/2 + Y + (H - ctx.lineWidth) * (j/nRows)
      const x1 = X
      const x2 = X + W
      line(x1, y, x2, y)
    }
    ctx.stroke()
  }
}

function drawRawOscilloscope(xy, ctx, opts = {}) {
  const dpr = canvasFixDpi(ctx)

  const color = opts.color || 'rgba(0,0,0,1)'
  const lineWidth = 5 * dpr
  const scaling = 1

  const { x: xVals, y: yVals } = xy
  const { x: dx, y: dy, w, h, X, Y, W, H } = convertRect({ x: 0, y: 0, w: -0, h: -0 }, ctx, 24)

  const s = (v, c, l) => c + l * v * scaling

  const N = Math.min(xVals.length, yVals.length)

  const offset = Math.floor(opts.offset || 0)
  const length = Math.floor(opts.length || N)
  const fullLength = Math.floor(opts.fullLength || N)
  const end = offset + length
  const increment = Math.ceil(opts.increment || 1)

  if (offset === 0) {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
    ctx.fillStyle = 'rgba(255,255,255)'
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)
  } else {
    ctx.fillStyle = 'rgba(255,255,255,0.1)'
    ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height)
  }

  // drawGrids(ctx, { X, Y, W, H }, [
  //   {
  //     nDivs: 10 / scaling,
  //     color: 'hsl(0deg, 0%, 90%)',
  //     width: 1,
  //     excludeBorders: 1,
  //   },
  //   {
  //     nDivs: 2 / scaling,
  //     color: 'hsl(0deg, 0%, 60%)',
  //     width: 1,
  //     excludeBorders: 1,
  //   },
  // ])

  /* ctx.beginPath()
  ctx.lineCap = 'round'
  ctx.lineJoin = 'round'
  ctx.lineWidth = lineWidth
  ctx.strokeStyle = color
  for (let t = offset; t < end; t++) {
    const x = s(xVals[t], dx + w / 2, w / 2)
    const y = s(yVals[t], dy + h / 2, -h / 2)
    if (t === offset) {
      ctx.moveTo(x, y)
    } else {
      ctx.lineTo(x, y)
    }
  }
  ctx.stroke() */

  ctx.lineCap = 'round'
  ctx.lineJoin = 'round'
  ctx.lineWidth = lineWidth
  ctx.strokeStyle = color
  // ctx.strokeStyle = `rgba(0,0,0,0.1)`
  // ctx.globalCompositeOperation = 'source-over'
  let prev = [0, 0]
  const widthFalloff = 0.01 * (opts.widthFalloffMult||1)
  const lw = lineWidth //* Math.exp(-length/4410)
  for (let t = offset; t < end; t += increment) {
    const x = s(xVals[t], dx + w / 2, w / 2)
    const y = s(yVals[t], dy + h / 2, -h / 2)
    if (t === offset) {
      ctx.moveTo(x, y)
    } else {
      const d = Math.hypot(x - prev[0], y - prev[1])
      const o = Math.exp(-widthFalloff * d / increment)
      if (o > 0.001) {
        ctx.beginPath()
        ctx.moveTo(...prev)

        // const h = 360 * t / fullLength
        // ctx.strokeStyle = `hsla(${h}deg,100%,50%,${o})`
        ctx.strokeStyle = `rgba(0,0,0,${o})`
        ctx.lineWidth = lw * o
        ctx.lineTo(x, y)
        ctx.stroke()
      }
    }
    prev = [x, y]
  }
  // ctx.globalCompositeOperation = 'source-over'
}

function drawOscilloscope(xy, ctx, opts = {}) {
  // function conv(xy, ctx, opts) {
  //   const { maxFreqHint = 100 } = opts
  //   const duration = 2 // app.freqMult
  //   let sampleRate = maxFreqHint / app.freqMult
  //   let nSamples = Math.floor(duration * sampleRate)
  //   while (nSamples > 400) {
  //     nSamples = Math.round(nSamples / 2)
  //     sampleRate = Math.round(sampleRate / 2)
  //   }
  //   const { x: fx, y: fy } = xy
  //   const xy2 = {
  //     x: new Float32Array(nSamples),
  //     y: new Float32Array(nSamples),
  //   }
  //   const k = 1 // Math.max(fx.length, fy.length)
  //   ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)
  //   for (let iSample = 0; iSample < nSamples; iSample++) {
  //     const t = app.freqMult * iSample / sampleRate
  //     const x = fx.reduce((s, f) => s + f(t), 0) / k
  //     const y = fy.reduce((s, f) => s + f(t), 0) / k
  //     xy2.x[iSample] = x
  //     xy2.y[iSample] = y
  //   }
  //   const opts2 = {
  //     ...opts,
  //     widthFalloffMult: app.freqMult,
  //   }
  //   return drawRawOscilloscope(xy2, ctx, opts2)
  // }
  // if (app.started) {
  //   return conv(xy, ctx, opts)
  // }

  const dpr = canvasFixDpi(ctx)

  const mode = opts.mode || ctx.canvas.getAttribute('mode') || 'full'
  const p = (opts.maxTime === 0) ? 0 : (opts.maxTime || 1)
  const color = opts.color || 'rgba(0,0,0,0.9)'

  const lineWidth = 5 * dpr

  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)

  const { x: fx, y: fy } = xy
  const { x: dx, y: dy, w, h, X, Y, W, H } = convertRect({ x: 0, y: 0, w: -0, h: -0 }, ctx, 24)
  const k = 1 // Math.max(fx.length, fy.length)
  const scaling = 1

  const s = (t, fns, c, l) => {
    const v = fns.reduce((s, f) => s + f(t), 0) / k
    return c + l * v * scaling
  }

  ctx.beginPath()
  ctx.lineCap = 'round'
  ctx.lineJoin = 'round'
  ctx.lineWidth = lineWidth
  ctx.strokeStyle = color
  let px, py
  for (let t = 0; t < p; t += 0.0001) {
    const x = s(t, fx, dx + w / 2, w / 2)
    const y = s(t, fy, dy + h / 2, -h / 2)

    if (t === 0) {
      ctx.moveTo(x, y)
    } else {
      const dist = Math.hypot(x - px, y - py) / W
      if (dist > 0.9) {
        ctx.stroke()
        ctx.moveTo(x, y)
      } else {
        ctx.lineTo(x, y)
      }
    }
    px = x
    py = y
  }
  ctx.stroke()

  /* ctx.lineCap = 'round'
  ctx.lineJoin = 'round'
  ctx.lineWidth = lineWidth
  ctx.strokeStyle = color
  ctx.strokeStyle = `rgba(0,200,100,0.1)`
  let prev = [0, 0]
  for (let t = 0; t < 5 * p; t += 0.01) {
    const x = s(t, fx, dx + w / 2, w / 2)
    const y = s(t, fy, dy + h / 2, -h / 2)
    const d = Math.hypot(x - prev[0], y - prev[1])
    if (t === 0) {
      ctx.moveTo(x, y)
    } else {
      ctx.beginPath()
      ctx.moveTo(...prev)
      ctx.lineTo(x, y)
      ctx.stroke()
    }
    prev = [x, y]
  } */

  /* for (let t = 0; t <= p * app.freqMult; t += 0.001 * app.freqMult) {
    ctx.beginPath()
    const op = t / (p * app.freqMult)
    ctx.fillStyle = `hsla(${240+60*op},100%,${30+60*(1-op)}%)`
    const x = s(t, fx, dx + w / 2, w / 2)
    const y = s(t, fy, dy + h / 2, -h / 2)
    ctx.arc(x, y, op * lineWidth/2, 0, 2*PI)
    ctx.fill()
  } */

  // drawGrids(ctx, { X, Y, W, H }, [
  //   {
  //     nDivs: 10 / scaling,
  //     color: 'hsl(0deg, 0%, 90%)',
  //     width: 1,
  //     excludeBorders: 1,
  //   },
  //   {
  //     nDivs: 2 / scaling,
  //     color: 'hsl(0deg, 0%, 60%)',
  //     width: 1,
  //     excludeBorders: 1,
  //   },
  // ])
}


function renderSection(_parent, id, spec, opts = {}) {
  const rerenderSection = () => renderSection(_parent, id, spec, opts)
  const playSound = () => {
    for (const k in app.synths) {
      if (!k.startsWith(id)) continue
      const s = app.synths[k]
      const freq = Number(s.frequency.value)
      s.triggerAttackRelease(freq, 0.75)
    }
  }
  assert(spec.oscGroups)
  spec.rerender = rerenderSection
  spec.playSound = playSound

  const parent = (_parent.id === id) ? _parent.parentElement : _parent

  let el = document.querySelector('#' + id)
  if (!el || el.innerHTML.trim() === '') {
    const newEl = getTemplate('om-section')
    newEl.id = id

    if (el) {
      el.replaceWith(newEl)
    } else {
      parent.append(newEl)
    }
    el = newEl

    const btn = el.querySelector('.om-play-button')
    btn.addEventListener('click', async () => {
      await app.start()
      if (!spec.callback || spec.callback === 'synth') {
        playSound()
      } else if (typeof spec.callback === 'function') {
        spec.callback(spec, rerenderSection)
      }
    })
  }

  const groupsEl = el.querySelector('[slot-key="groups"]')
  assert(groupsEl)
  for (let i = 0; i < spec.oscGroups.length; i++) {
    const grp = spec.oscGroups[i]
    renderOscGroup(groupsEl, id + '_g' + i, grp, { ...opts, rerenderSection })
  }

  const innerOscilloscopeWrapper = el.querySelector('.om-section--oscilloscope')
  const oscilloscopeWrapper = spec.oscilloscopeSelector
    ? document.querySelector(spec.oscilloscopeSelector) || innerOscilloscopeWrapper
    : innerOscilloscopeWrapper

  if (oscilloscopeWrapper !== innerOscilloscopeWrapper) {
    innerOscilloscopeWrapper.setAttribute('hidden', 'hidden')
  }
  if (spec.showOscilloscope) {
    oscilloscopeWrapper.removeAttribute('hidden')

    if (oscilloscopeWrapper.childElementCount === 0) {
      oscilloscopeWrapper.appendChild(getTemplate('om-oscilloscope', {}))
    }

    const oscilloscope = oscilloscopeWrapper.querySelector('canvas.om-oscilloscope')
    const ctx = oscilloscope.getContext('2d')
    const flt = (c) => spec.oscGroups
      .flatMap(g => g.oscillators)
      .filter(o => o.channel === c || o.channel === 'C')
      // .map(o => getFuncForSignal(o.type, o.freq, o.phase))
      .map(o => {
        const gain = typeof o.gain === 'number' ? o.gain : 1
        const fn = getFuncForSignal(o.type, o.freq * 2 * PI, o.phase)
        if (gain === 1) return fn
        return (...args) => gain * fn(...args)
      })

    const oscs = {
      x: flt('L'),
      y: flt('R'),
    }
    drawOscilloscope(oscs, ctx, {
      maxTime: 1,
      maxFreqHint: 2 * app.freqMult * Math.max(
        ...spec.oscGroups.map(
          g => Math.max(...g.oscillators.map(
            o => o.freq))))
    })
  } else {
    oscilloscopeWrapper.setAttribute('hidden', 'hidden')
  }
}

function renderOscGroup(parent, id, spec, opts) {
  assert(spec.oscillators)
  const rerenderOscGroup = () => renderOscGroup(parent, id, spec, opts)

  let el = parent.querySelector('#' + id)
  if (!el) {
    el = getTemplate('om-osc-group')
    el.id = id
    parent.append(el)
  }

  if (spec.color) {
    el.style.setProperty('--color', spec.color)
  }

  const container = el.querySelector('[slot-key="oscillators"]')
  for (let i = 0; i < spec.oscillators.length; i++) {
    const osc = spec.oscillators[i]
    renderOsc(container, id + '_o' + i, osc, { ...opts, rerenderOscGroup })
  }
}

function renderOsc(parent, id, spec, opts) {
  assert(parent)
  assert(id)
  assert(spec)
  const rerender = () => renderOsc(parent, id, spec, opts)

  const { type, phase, desc = '', gain = 1 } = spec
  const freq = spec.freq * app.freqMult

  let el = parent.querySelector('#' + id)
  if (!el) {
    el = getTemplate('om-osc')
    el.id = id
    parent.append(el)

    if (spec.config && spec.config.menu) {
      const menu = el.querySelector('.om-osc-details')
      if (spec.config.menu === 'open') {
        menu.setAttribute('open', 'open')
      } else if (spec.config.menu === 'close') {
        menu.removeAttribute('open')
      } else if (spec.config.menu === 'none') {
        // no menu, only summary
        menu.replaceWith(getTemplate('om-osc-details-summary', {}))
      } else if (spec.config.menu === 'force-open') {
        // no menu, only summary
        menu.replaceWith(getTemplate('om-osc-details-content', {}))
      }
    }

    const inputs = el.querySelectorAll(`:is(input,select)[slot-key]`)
    inputs.forEach(inp => {
      const upd = (e) => {
        const k = inp.getAttribute('slot-key')
        const v = inp.value
        if (k === 'freq') {
          spec.freq = Number(v) / app.freqMult
        }
        else if (k === 'phase') { spec.phase = Number(v) }
        else if (k === 'type') { spec.type = String(v) }
        else if (k === 'channel') { spec.channel = String(v) }
        else if (k === 'gain') { spec.gain = Number(v) }
        // rerender()
        if (e.type === 'input') {
          if (k === 'freq' || k === 'type' || k === 'channel' || k === 'gain') {
            const s = app.synths[id]
            if (s) {
              s.triggerAttackRelease(spec.freq, 0.05)
            }
          }
        }
        opts.rerenderSection()
      }
      inp.addEventListener('input', upd)
      inp.addEventListener('change', upd)
    })

    const canvas = el.querySelector('canvas')
    canvas?.addEventListener('click', (e) => {
      const s = app.synths[id]
      if (s) {
        s.triggerAttackRelease(spec.freq, 0.15)
      }
    })
  }

  el.style.setProperty('--color', spec.color)
  setSlot(el, 'freq', round(freq))
  setSlot(el, 'type', type)
  setSlot(el, 'phase', round(spec.phase))
  setSlot(el, 'desc', desc)
  setSlot(el, 'channel', spec.channel)
  setSlot(el, 'gain', gain)

  setSlot(el, 'fmt(type)', formatOscType(type, Math.round(spec.phase)))
  setSlot(el, 'fmt(channel)', formatChannel(spec.channel))

  if (spec.config && spec.config.controls) {
    for (const k of ['freq', 'type', 'phase', 'channel', 'desc', 'gain']) {
      if (spec.config.controls.includes(k)) {
        showSlotParent(el, k)
      } else {
        hideSlotParent(el, k)
      }
    }
  }

  if (spec.config && spec.config.types) {
    const select = el.querySelector('select[slot-key="type"]')
    if (select) {
      const allowed = spec.config.types
      const options = select.options
      for (const opt of options) {
        if (allowed.includes(opt.value)) {
          opt.style.display = 'block'
        } else {
          opt.style.display = 'none'
        }
      }
    }
  }

  const volume = 20 * Math.log(gain)

  const canvas = el.querySelector('canvas')
  const ctx = canvas.getContext('2d')

  if (app.started && !app.synths[id]) {
    app.synths[id] = new Tone.Synth()
  }

  if (app.started && app.synths[id]) {
    const synth = app.synths[id]
    synth.oscillator.baseType = type
    synth.oscillator.frequency.value = freq
    synth.oscillator.phase = phase
    synth.volume.value = volume
    synth.envelope.attack = 0.01
    synth.envelope.decay = 0
    synth.envelope.sustain = 1
    synth.envelope.release = 0.5

    const chan = app[spec.channel] || app.C
    synth.disconnect().connect(chan)
    drawOscillator(synth.oscillator, ctx, spec.color, spec.gain)
  } else {
    drawOscillator({
      baseType: type,
      frequency: { value: freq },
      phase,
    }, ctx, spec.color, spec.gain)
  }
}

const makePhaseShifter = ({ duration = 1000, maxPhase = 360 } = {}) => ({
  _phaseShifter: { running: false, t: 0, T: duration },
  callback: (spec, rerender) => {
    const synths = []

    spec.oscGroups?.forEach(
      g => g.oscillators?.forEach(
        o => {
          const chan = app[o.channel] || app.C
          const s = new Tone.Synth().connect(chan)
          s.oscillator.type = o.type
          s.oscillator.phase = o.phase
          s.envelope.attack = 0.1
          s.envelope.decay = 0
          s.envelope.release = 0.1
          s.triggerAttackRelease(o.freq * app.freqMult, spec._phaseShifter.T / 1000)
          synths.push(s)
        }))

    const f = (realFrameTime) => {
      if (!spec._phaseShifter.running) {
        spec._phaseShifter.running = true
        spec._phaseShifter._startTime = realFrameTime

        spec.oscGroups?.forEach(
          g => g.oscillators?.forEach(
            o => { o._initialPhase = o.phase }))
        requestAnimationFrame(f)
        return // console.info('early return: phase shifter started')
      }

      const frameTime = realFrameTime - spec._phaseShifter._startTime
      spec._phaseShifter.t = frameTime

      if (spec._phaseShifter.t >= spec._phaseShifter.T) {
        spec._phaseShifter.running = false
        spec._phaseShifter.t = 0
        spec.oscGroups?.forEach(
          g => g.oscillators?.forEach(
            o => { o.phase = o._initialPhase }))
        rerender()
        setTimeout(() => {
          synths.forEach(s => s.dispose())
        }, 1000)
        return
      }

      const pct = spec._phaseShifter.t / spec._phaseShifter.T

      spec.oscGroups?.forEach(
        g => g.oscillators?.forEach(
          o => {
            o.phase = (o._initialPhase + pct * maxPhase) % 360
          }))
      rerender()
      requestAnimationFrame(f)
    }
    requestAnimationFrame(f)
  },
})

const section1_spec = {
  oscGroups: [{
    oscillators: [{
      freq: 440, type: 'sine', phase: 0,
      desc: 'A 440 Hz sine wave.',
      channel: 'C',
      color: OMColors.deepPurple,
      gain: 1,
      config: {
        menu: 'none',
        controls: ['freq', 'type', 'desc'],
        types: ['sine'],
      },
    }],
  }],
  showOscilloscope: false,
}

const section2_spec = {
  oscGroups: [{
    oscillators: [{
      freq: 250, type: 'sawtooth', phase: 0,
      desc: 'Early synthesizers made simple sounds.',
      channel: 'C',
      color: OMColors.deepOrange,
      gain: 1,
      config: {
        menu: 'force-open',
        controls: ['freq', 'type', 'desc'],
      },
    }],
  }],
  showOscilloscope: false,
}

const section3_spec = {
  // ...makePhaseShifter({ duration: 500 }),
  oscGroups: [{
    oscillators: [{
      freq: 100, type: 'sawtooth', phase: 0,
      desc: '',
      channel: 'C',
      color: OMColors.red,
      gain: 2/3,
      config: {
        controls: ['freq', 'type', 'desc'],
      }
    }, {
      freq: 250, type: 'square', phase: 0,
      desc: '',
      channel: 'C',
      color: OMColors.cyan,
      gain: 2/3,
      config: {
        controls: ['freq', 'type', 'desc'],
      }
    }, {
      freq: 500, type: 'triangle', phase: 0,
      desc: '',
      channel: 'C',
      color: OMColors.blue,
      gain: 2/3,
      config: {
        controls: ['freq', 'type', 'desc'],
      }
    }],
  }],
  showOscilloscope: false,
}

const section4_spec = {
  // ...makePhaseShifter({ duration: 500 }),
  oscGroups: [{
    oscillators: [{
      freq: 800, type: 'sine', phase: 0,
      // desc: 'A sine wave that plays in the left channel.',
      channel: 'L',
      color: OMColors.blue,
      config: {
        controls: ['freq', 'type', 'desc', 'channel', 'gain'],
      },
    }],
  }, {
    oscillators: [{
      freq: 400, type: 'sine', phase: 90,
      // desc: 'A cosine wave that plays in the right channel.',
      channel: 'R',
      color: OMColors.orange,
      config: {
        controls: ['freq', 'type', 'desc', 'channel', 'gain'],
      },
    }],
  }],
  showOscilloscope: true,
}

const section5_spec = {
  // ...makePhaseShifter(),
  oscGroups: [{
    oscillators: [
      {
        freq: 500, type: 'sine', phase: 0,
        desc: '',
        channel: 'L',
        gain: 0.5,
        color: OMColors.purple,
      },
      {
        freq: 100, type: 'sine', phase: 0,
        desc: '',
        channel: 'L',
        gain: 0.5,
        color: OMColors.blue,
      },
      {
        freq: 500, type: 'sine', phase: 90,
        desc: '',
        channel: 'R',
        gain: 0.5,
        color: OMColors.orange,
      },
      {
        freq: 100, type: 'sine', phase: 90,
        desc: '',
        channel: 'R',
        gain: 0.5,
        color: OMColors.red,
      },
    ],
  }],
  showOscilloscope: true,
  // oscilloscopeSelector: '#om-section-5-oscilloscope',
}

const om_sections = {
  'om-section-1': section1_spec,
  'om-section-2': section2_spec,
  'om-section-3': section3_spec,
  'om-section-4': section4_spec,
  'om-section-5': section5_spec,
}

function renderAll() {
  for (const [id, spec] of Object.entries(om_sections)) {
    const el = document.getElementById(id)
    if (el) renderSection(el, id, spec)
  }
}


function parseExpr(expr, scope = {}) {
  const oscillators = []
  const req = (x) => { throw new TypeError('required parameter', x) }

  const splitArgs = (args) =>
    bifilter(args, a => a.type === 'signal')
      .map(a => a.map(x => x.value))

  // const cache = new Map()
  const funcs = new Map()
  const globalScope = scope
  const localScopes = []
  const errors = []

  function setVariable(name, value) {
    if (name === 'XY') {
      let x = Value(0), y = Value(0)
      if (value.type === 'list') {
        x = value.value[0]
        y = value.value[1]
      } else if (value.type === 'object') {
        x = value.value.x
        y = value.value.y
      } else if (value.type === 'value') {
        x = value
      } else if (value.type === 'signal') {
        x = value
      }
      globalScope.x = x
      globalScope.y = y
    }

    for (let i = localScopes.length - 1; i >= 0; i--) {
      const s = localScopes[i]
      if (name in s) {
        s[name] = value
        return
      }
    }
    if (name in globalScope) {
      globalScope[name] = value
      return
    }
    if (localScopes.length > 0) {
      localScopes[localScopes.length - 1][name] = value
      return
    }
    globalScope[name] = value
  }

  function getVariable(name) {
    for (let i = localScopes.length - 1; i >= 0; i--) {
      const s = localScopes[i]
      if (name in s)
        return s[name]
    }
    if (name in globalScope)
      return globalScope[name]
  }

  function it(node) {
    return iterateNodeWithErrors(node)
  }

  function subparse(text) {
    return it(math.parse(text))
  }

  const registerObject = (o) => {
    globalScope['$__' + o.id] = o
    return o
  }
  const Value = (value) => registerObject({
    id: 'v:'+value,
    type: 'value',
    value,
  })
  const Signal = (value) => registerObject({
    id: 's:'+value.name+'_'+Math.random().toString(36).substr(6),
    type: 'signal',
    value,
  })
  const List = (value) => registerObject({
    id: 'l:#'+value.length+Math.random().toString(36).substr(2),
    type: 'list',
    value,
  })


  const memoIdStore = new Map()
  const memoId = (name, func) => (..._args) => {
    // always compute if no argument
    if (_args.length === 0) return func()

    let args = _args
    if (_args.length === 1 && Array.isArray(_args[0])) {
      args = _args[0]
    }

    const NO_MEMO = '!!!NO_MEMO!!!'
    const ids = args.map(x => {
      if (typeof x === 'string')
        return x
      if (typeof x === 'number')
        return String(x)
      if (x && typeof x.id === 'string')
         return x.id
      return NO_MEMO
    })

    const key = name + '(' + ids.join('|') + ')'
    if (key.includes(NO_MEMO)) {
      // console.log('NO MEMO', key)
      return func(..._args)
    }

    if (memoIdStore.has(key)) {
      // console.log('<- memoize.GET', key)
      return memoIdStore.get(key)
    } else {
      // console.log('-> memoize.SET', key)
      const r = func(..._args)
      memoIdStore.set(key, r)
      return r
    }
  }

  const connectOrSetValue = (v, x) => {
    if (x.type === 'signal')
      x.value.connect(v)
    else
      v.value = x.value
  }

  const add = memoId('add', (args) => {
    const [signals, values] = splitArgs(args)
    const valSum = values.reduce((a,b) => a+b, 0)
    if (signals.length === 0) {
      return Value(valSum)
    } else if (valSum === 0 && signals.length === 1) {
      return Signal(signals[0])
    }
    const sum = new Tone.Add(valSum)
    signals.forEach(s => s.connect(sum))
    return Signal(sum)
  })
  const neg = memoId('neg', (arg) => {
    if (arg.type === 'value') {
      return Value(-arg.value)
    }
    const n = new Tone.Negate()
    arg.value.connect(n)
    return Signal(n)
  })
  const osc = memoId('osc', (_type, freq, _phase) => {
    if (_phase.type === 'value') {
    } else {
      throw new TypeError('The `phase` argument should be a value.')
    }

    let phase = -_phase.value
    let type = _type
    if (type === 'cos' || type === 'cosine') {
      type = 'sine'
      phase = phase - 90
    }
    /* if (type === 'ramp') {
      type = 'sawtooth'
      phase = add([phase, Value(180)])
    } */
    if (type === 'sin') type = 'sine'
    if (type === 'sqr') type = 'square'
    if (type === 'saw') type = 'sawtooth'
    if (type === 'tri') type = 'triangle'

    let o
    if (freq.type === 'value') {
      o = new Tone.Oscillator(freq.value, type)
    } else {
      o = new Tone.Oscillator()
      freq.value.connect(o.frequency)
    }
    o.phase = phase
    o.baseType = type
    oscillators.push(o)
    return Signal(o)
  })
  const mul = memoId('mul', (args) => {
    const [signals, values] = splitArgs(args)
    const valProd = values.reduce((a,b) => a*b, 1)

    if (valProd === 0) {
      return Value(0)
    } else if (signals.length === 0) {
      return Value(valProd)
    } else if (valProd === 1 && signals.length === 1) {
      return Signal(signals[0])
    } else if (signals.length === 1) {
      const m = new Tone.Multiply(valProd)
      signals[0].connect(m)
      return Signal(m)
    }

    const Mul = (a, b) => {
      const m = new Tone.Multiply()
      a.connect(m)
      b.connect(m.factor)
      return m
    }
    const recMul = ([a, ...tail]) => {
      if (tail.length === 0) {
        return a
      } else {
        return Mul(a, recMul(tail))
      }
    }
    const mult = new Tone.Multiply(valProd)
    const m = recMul(signals)
    m.connect(mult)
    return Signal(mult)
  })
  const pow = memoId('pow', (args) => {
    assert(args.length === 2)
    if (args[1].type !== 'value') {
      throw new TypeError('`exponent` should be a value')
    }
    if (args[0].type === 'value') {
      return Value(args[0].value ** args[1].value)
    }
    const pow = new Tone.Pow(args[1].value)
    args[0].value.connect(pow)
    return Signal(pow)
  })
  const alignedPulse = memoId('alignedPulse', (freq, dutyCycle = Value(0.5), phase = Value(0)) => {
    if (phase.type !== 'value') throw new TypeError('alignedPulse: The `phase` argument should be a value.')
    if (dutyCycle.type !== 'value') throw new TypeError('alignedPulse: The `duty cycle` argument should be a value.')
    return pwm(freq, dutyCycle, Value(phase.value + 90 - 180 * dutyCycle.value))
  })
  const pwm = memoId('pwm', (freq, dutyCycle, phase) => {
    const constructorOpts = {}

    if (freq.type === 'value') {
      constructorOpts.frequency = freq.value
    }
    if (dutyCycle.type === 'value') {
      if (dutyCycle.value >= 0 && dutyCycle.value <= 1) {
        constructorOpts.width = 2 * dutyCycle.value - 1
      } else {
        throw new TypeError('The `duty cycle` argument should be a value between 0 and 1, or a signal.')
      }
    }
    if (phase.type === 'value') {
      constructorOpts.phase = -phase.value
    } else {
      throw new TypeError('The `phase` argument should be a value between -360 and 360.')
    }

    const o = new Tone.PulseOscillator(constructorOpts)

    if (freq.type === 'signal') {
      freq.value.connect(o.frequency)
    }
    if (dutyCycle.type === 'signal') {
      dutyCycle.value.connect(o.width)
      // add([ mul([ Value(2), dutyCycle ]), Value(-1) ]).value.connect(o.width)
    }

    oscillators.push(o)
    return Signal(o)
  })
  const div = memoId('div', (args) => {
    assert(args.length === 2)
    if (args[0].type === 'value' && args[1].type === 'value') {
      return Value(args[0].value / args[1].value)
    } else if (args[1].type === 'value') {
      const div = new Tone.Multiply(1 / args[1].value)
      args[0].value.connect(div)
      return Signal(div)
    } else {
      const inv = new Tone.Pow(-1)
      args[1].value.connect(inv)
      return mul([args[0], Signal(inv)])
    }
  })
  const gt = memoId('gt', (x, y) => {
    if (x.type === 'value' && y.type === 'value') {
      return x.value > y.value ? Value(1) : Value(0)
    }
    else if (x.type === 'value' && y.type === 'signal') {
      const node = new Tone.GreaterThan()
      new Tone.Signal(x.value).connect(node)
      y.value.connect(node.comparator)
      return Signal(node)
    }
    else if (x.type === 'signal' && y.type === 'value') {
      const node = new Tone.GreaterThan(y.value)
      x.value.connect(node)
      return Signal(node)
    }
    else if (x.type === 'signal' && y.type === 'signal') {
      const node = new Tone.GreaterThan()
      x.value.connect(node)
      y.value.connect(node.comparator)
      return Signal(node)
    }
    return Value(-1)
  })
  const geq = memoId('geq', (x, y) => {
    if (x.type === 'value' && y.type === 'value') {
      return x.value >= y.value ? Value(1) : Value(0)
    }
    return gt(x, y)
  })
  const gtz = memoId('gtz', (x) => {
    if (x.type === 'value') {
      return x.value > 0 ? Value(1) : Value(0)
    } else {
      const n = new Tone.GreaterThanZero()
      x.value.connect(n)
      return Signal(n)
    }
  })
  const oneMinusX = memoId('omx', (x) => add([Value(1), neg(x)]))
  const noise = () => {
    const o = new Tone.Noise()
    oscillators.push(o)
    return Signal(o)
  }
  const atg = memoId('atg', (s) => {
    if (s.type === 'value') return Value(0.5 * s.value + 0.5)
    const atg = new Tone.AudioToGain()
    s.value.connect(atg)
    return Signal(atg)
  })
  const gta = memoId('gta', (s) => {
    if (s.type === 'value') return Value(2 * s.value - 1)
    const gta = new Tone.GainToAudio()
    s.value.connect(gta)
    return Signal(gta)
  })

  const lfo = memoId('lfo', (_type, freq, phase, min, max) => {
    const lfo = new Tone.LFO({
      type: _type,
    })
    assert(phase.type === 'value')
    connectOrSetValue(lfo.frequency, freq)
    connectOrSetValue(lfo.phase, phase)
    connectOrSetValue(lfo.min, min)
    connectOrSetValue(lfo.max, max)
    oscillators.push(lfo)
    return Signal(lfo)
  })

  const bufSource = (arrayF32, opts = {}) => {
    const actx = Tone.getContext()
    const buffer = actx.createBuffer(1, arrayF32.length, actx.sampleRate)
    buffer.getChannelData(0).set(arrayF32)
    const src = new Tone.BufferSource({
      url: buffer,
      ...opts,
    })
    oscillators.push(src)
    return Signal(src)
  }

  const square = memoId('square', (...args) => {
    const [
      freq = Value(1/scope.duration),
      phase = Value(0),
    ] = args
    return gta(gtz(osc('sine', freq, phase)))
    // if (freq.type === 'signal' || phase.type === 'signal' || freq.value >= 1024) {
    //     return osc('square', freq, phase)
    // }
    // const actx = Tone.getContext()
    // const nSamples = Math.floor(actx.sampleRate)
    // let phi = Math.round(nSamples * (phase.value / 360))
    // while (phi < 0) { phi += nSamples }
    // while (phi >= nSamples) { phi -= nSamples }
    // const c0 = new Float32Array(nSamples)
    // for (let i = 0; i < nSamples; i++) {
    //   const j = (i - phi + nSamples) % nSamples
    //   c0[j] = (i < nSamples/2) ? +1 : -1
    // }
    // const src = bufSource(c0, { loop: true })
    // connectOrSetValue(src.value.playbackRate, freq)
    // return src
  })

  const grain = (grainSize, rate, arrayF32, opts = {}) => {
    const actx = Tone.getContext()
    const buffer = actx.createBuffer(1, arrayF32.length, actx.sampleRate)
    buffer.getChannelData(0).set(arrayF32)
    const src = new Tone.GrainPlayer({
      url: buffer,
      ...opts,
    })
    connectOrSetValue(src.grainSize, grainSize)
    connectOrSetValue(src.playbackRate, rate)
    oscillators.push(src)
    return Signal(src)
  }

  const gain = (vol, inp) => {
    assert(inp.type === 'signal')
    const g = new Tone.Gain()
    inp.value.connect(g)
    connectOrSetValue(g.gain, vol)
    return Signal(g)
  }

  const abs = memoId('abs', (x) => {
    if (x.type === 'value')
      return Value(Math.abs(x.value))
    const n = new Tone.Abs()
    x.value.connect(n)
    return Signal(n)
  })

  const ramp = memoId('ramp', (freq) => {
    if (freq.type === 'signal' || freq.value >= 1024) {
      return osc('sawtooth', freq, Value(180))
    }

    const actx = Tone.getContext()
    const nSamples = actx.sampleRate

    const c0 = new Float32Array(nSamples)
    for (let i = 0; i < nSamples; i++) {
      c0[i] = 2 * i / nSamples - 1
    }

    const src = bufSource(c0, { loop: true })
    connectOrSetValue(src.value.playbackRate, freq)
    return src
  })

  const waveshaper = (mapping, length = 2**16) => {
    return Signal(new Tone.WaveShaper({
      length,
      mapping,
    }))
  }

  const lerp = (args) => {
    assert(args.length === 3)
    const [t, a, b] = args
    return add([
      mul([t, b]),
      mul([oneMinusX(t), a])
    ])
  }

  const iff = (args) => {
    assert(args.length === 2 || args.length === 3)
    const [c, a, b = Value(0)] = args
    const t = gt(c, Value(0.5))
    return lerp([t, b, a])
  }

  const SET_POS_XY = (_x, _y, _w, _h, leftChan, rightChan) => {
    if ([_x, _y, _w, _h].some(x => typeof x === 'object' && x.type === 'signal')) {
      throw new TypeError('SET_POS_XY function: x, y, w, h arguments should be numbers')
    }
    const [x, y, w, h] = [_x, _y, _w, _h].map(z => typeof z === 'number' ? Value(z) : z)
    const l = add([x, mul([leftChan, w])])
    const r = add([y, mul([rightChan, h])])
    return List([l, r])
  }

  const makeGridRects = function* (nw = 2, nh = 2, pad = 0) {
    const w = 2 * (1 / nw) - 2 * pad
    const h = 2 * (1 / nh) - 2 * pad
    for (let j = 0; j < nh; j++) {
      const y = 2 * (j / nh) - 1 + (h/2) + pad
      for (let _i = 0; _i < nw; _i++) {
        // alternate the direction of the row
        const i = _i // (j % 2 === 0) ? _i : (nw - 1) - _i
        const x = 2 * (i / nw) - 1 + (w/2) + pad
        yield [x, -y, w/2, h/2]
      }
    }
  }

  const SHARE = (freq, ...items) => {
    if (freq.type !== 'value') {
      throw new TypeError('SHARE function: 1st argument should be a frequency value')
    }
    const n = items.length
    const dutyCycle = Value(1 / n)
    const ph = (i) => Value(360 * (i/n))
    return add(items.map((a, i) => {
      const sel = gtz(pwm(freq, dutyCycle, ph(i)))
      return mul([sel, a])
    }))
  }

  const GRID_XY = (freq, nw, nh, pad, ...lists) => {
    if ((!freq) || freq.type !== 'value') {
      throw new TypeError('GRID function: 1st argument should be a frequency value')
    }
    if ((!nw) || nw.type !== 'value') {
      throw new TypeError('GRID function: 2nd argument (number of columns) should be an integer')
    }
    if ((!nh) || nh.type !== 'value') {
      throw new TypeError('GRID function: 3rd argument (number of rows) should be an integer')
    }
    if ((!pad) || pad.type !== 'value') {
      throw new TypeError('GRID function: 3rd argument (padding) should be an integer')
    }
    const rects = [...makeGridRects(nw.value, nh.value, pad.value)]
    const outX = []
    const outY = []
    let i = 0
    const N = nw.value * nh.value // lists.length
    for (let i = 0; i < N; i++) {
      // const list = (i > lists.length) ? lists[lists.length - 1] : lists[i] // repeat last value
      const list = lists[i % lists.length] // wrap around if needed
      if (list.type === 'value') { continue } // skip
      const rect = rects[i]

      /* localScopes.push({
        x: Value(0),
        y: Value(0),
      })
      const o = it(funcs[funcName].expr)
      const { x: ox, y: oy } = localScopes.pop() */

      const ox = list.value[0] ?? list.value.x ?? 0
      const oy = list.value[1] ?? list.value.y ?? 0
      const [l, r] = SET_POS_XY(...rect, ox, oy).value
      outX.push(l)
      outY.push(r)
    }
    const o = List([
      SHARE(freq, ...outX),
      SHARE(freq, ...outY),
    ])
    return o
  }

  const _PERIODIC = (func, opts = {}) => (x) => {
    const {
      inputScale = 1,
      outputScale = 1,
      period = 1,
      nPeriods = 100,
    } = opts
    switch (x.type) {
      case 'value':
        return Value(func(x.value))
      case 'signal':
        const g = gain(Value(inputScale / nPeriods), x)
        const ws = waveshaper((v) => func(v * nPeriods * period))
        g.value.connect(ws.value)
        return ws
    }
  }

  // NOTE: do not scale 2*Math.PI
  const SIN = memoId('SIN', _PERIODIC(Math.sin))
  const COS = memoId('COS', _PERIODIC(Math.cos))
  const TAN = memoId('TAN', _PERIODIC(Math.tan))

  const EXP = memoId('EXP', (x) => {
    switch (x.type) {
      case 'value':
        return Value(Math.exp(x.value))
      case 'signal':
        const ws = waveshaper((v) => Math.exp(v))
        x.value.connect(ws.value)
        return ws
    }
  })
  const SINC = memoId('SINC', (x) => {
    switch (x.type) {
      case 'value':
        if (x.value === 0) { return Value(1) }
        return Value(Math.sin(x.value) / x.value)
      case 'signal':
        const ws = waveshaper((v) => v !== 0 ? Math.sin(v) / v : 1)
        x.value.connect(ws.value)
        return ws
    }
  })
  const builtinFunctions = {
    SIN,
    COS,
    TAN,
    EXP,
    SINC,
    GRID: GRID_XY,
    atg,
    gta,
    pulse: pwm,
    alignedPulse,
  }

  const oscillatorTypes = [
    "sin", "sine",
    "cos", "cosine",
    "sqr", "square",
    "saw", "sawtooth",
    "tri", "triangle",
  ]

  /* function iterateNodeWithCacheAndErrors(node) {
    const k = node.toString()
    if (k in cache) {
      console.info('cached', k)
      return cache[k]
    }

    const o = iterateNodeWithErrors(node)
    cache[k] = o
    if ((o.type !== 'signal' && o.type !== 'value') || o.value === null || o.value === undefined) {
      console.error('BUG: node iteration returned invalid result', o, node)
      throw new TypeError('BUG: node iteration returned invalid result')
    }
    return o
  } */

  function iterateNodeWithErrors(node) {
    try {
      return iterateNode(node)
    } catch (e) {
      errors.push(e)
      return Value(0)
    }
  }

  function iterateNode(node) {
    const args = node.args ? node.args.map(n => it(n)) : []
    if (node.type === 'FunctionNode') {
      const name = node.fn?.name
      if (oscillatorTypes.includes(name)) {
        const type = name

        if (type === 'sqr' || type === 'square') {
          return square(...args)
        }

        const [freq = req('freq'), phase = Value(0)] = args
        return osc(type, freq, phase)
      }
      else if (name === 'pwm' || name === 'pulse') {
        const [freq = req('freq'), dutyCycle = Value(0.5), phase = Value(0)] = args
        return pwm(freq, dutyCycle, phase)
      }
      else if (name === 'exp') {
        assert(args.length === 1)
        const x = args[0]
        const precision = 9
        function factorial(n, r = 1) {
          while (n > 0) { r *= n-- }
          return r
        }
        const part = (k) => {
          const a = []
          a.push(Value(1 / factorial(k)))
          for (let i = 0; i < k; i++) {
            a.push(x)
          }
          return mul(a)
        }
        /*
        x = 0.1 sin(f) (exp(cos(f)) - 2 cos(4f))
        y = 0.1 cos(f) (exp(cos(f)) - 2 cos(4f))
        */
        return add(Array.from({
          length: precision
        }, (_, k) => part(k)))
        // return pow([Value(Math.E), args[0]])
      }
      else if (builtinFunctions[name]) {
        return builtinFunctions[name](...args)
      }
      else if (name === 'FLOWER') {
        const [k = Value(3)] = args
        localScopes.push({
          flower$k: k,
        })
        subparse(`
          x = 0.45 sin(f) (cos(f) - 2 cos(flower$k f))
          y = 0.45 cos(f) (cos(f) - 2 cos(flower$k f)) - 0.5
        `)
        // FLOWER(1+5t/f)
        const { x, y } = localScopes.pop()
        globalScope.x = x
        globalScope.y = y
        return Value(0)
      }
      else if (name === 'ramp') {
        const [freq = Value(1/scope.duration)] = args
        return ramp(freq)
      }
      else if (name === 'ramp01') {
        const [freq = Value(1/scope.duration)] = args
        return atg(ramp(freq))
      }
      else if (name === 'share') {
        const [freq = Value(1/scope.duration), ...tail] = args
        // assert(freq.type === 'value')
        const n = tail.length
        const dutyCycle = Value(1 / n)
        const ph = (i) => Value(360 * (i/n))
        return add(tail.map((a, i) => {
          const sel = gtz(pwm(freq, dutyCycle, ph(i)))
          return mul([sel, a])
        }))
      }
      else if (name === 'noise') {
        return noise()
      }
      else if (name === 'if' || name === 'iff') {
        return iff(args)
      }
      else if (name === 'lerp') {
        return lerp(args)
      }
      else if (name === 'shareB') {
        const [freq = Value(1/scope.duration), ...tail] = args
        const n = tail.length
        assert(freq.type === 'value')
        if (n === 1) return tail[0]
        const ramp = atg(osc('saw', freq, Value(0)))
        const sel = (i) => {
          const a = i === 0     ? -99 :   i   / n
          const b = i === (n-1) ? +99 : (i+1) / n

          const w = new Tone.WaveShaper({
            length: 2**16,
            mapping: (val) => (val < a || val >= b) ? 0 : 1,
          })
          ramp.value.connect(w)
          return Signal(w)
        }
        return add(tail.map((a, i) => {
          return mul([sel(i), a])
        }))
      }
      else if (name === 'shareC') {
        const [freq = Value(1/scope.duration), ...tail] = args
        assert(freq.type === 'value')
        const n = tail.length
        const dutyCycle = Value(1 / n)
        const ph = (i) => Value(360 * (i/n))
        return add(tail.map((a, i) => {
          const sel = atg(pwm(freq, dutyCycle, ph(i)))
          return mul([sel, a])
        }))
      }
      else if (name === 'abs') {
        assert(args.length === 1)
        return abs(args[0])
      }
      else if (name === 'atg') {
        assert(args.length === 1)
        return atg(args[0])
      }
      /*else if (name === 'lforamp') {
        assert(args.length === 4)
        const [freq, phase, min, max] = args
        return lfo('sawtooth', freq, phase, min, max)
      }*/
      else if (name in funcs) {
        const fDef = funcs[name]
        const localScope = {}
        for (let i = 0; i < fDef.params.length; i++) {
          const p = fDef.params[i]
          const a = args[i]
          localScope[p] = a
        }

        localScopes.push(localScope)
        const o = it(fDef.expr)
        localScopes.pop()

        if (!globalScope.__nFuncCalls) { globalScope.__nFuncCalls = 0 }
        const i = ++globalScope.__nFuncCalls
        globalScope['__' + fDef.name + '#' + i + ':scope'] = localScope
        globalScope['__' + fDef.name + '#' + i + ':ret'] = o

        return o
      } else {
        console.warn('no such function:', name)
        return Value(0)
      }
    }
    else if (node.type === 'OperatorNode') {
      if (node.fn === 'add') {
        return add(args)
      } if (node.fn === 'subtract') {
        return add(args.map((a, i) => i === 0 ? a : neg(a)))
      } else if (node.fn === 'unaryMinus') {
        return neg(args[0])
      } else if (node.fn === 'unaryPlus') {
        return args[0]
      } else if (node.fn === 'multiply') {
        return mul(args)
      } else if (node.fn === 'mod') {
        assert(args.length === 2)
        const [x, y] = args
        assert(x.type === 'value')
        assert(y.type === 'value')
        return x.value % y.value
      } else if (node.fn === 'pow') {
        return pow(args)
      } else if (node.fn === 'divide') {
        return div(args)
      } else if (node.fn === 'larger') {
        assert(args.length === 2)
        return gt(args[0], args[1])
      } else if (node.fn === 'smaller') {
        assert(args.length === 2)
        return geq(args[1], args[0])
      } else if (node.fn === 'largerEq') {
        assert(args.length === 2)
        return geq(args[0], args[1])
      } else if (node.fn === 'smallerEq') {
        assert(args.length === 2)
        return gt(args[1], args[0])
      } else {
        console.warn('op?', node)
        return Value(0)
      }
    }
    else if (node.type === 'AssignmentNode') {
      assert(node.object.type === 'SymbolNode')
      const n = it(node.value)
      setVariable(node.object.name, n)
      return n
    }
    else if (node.type === 'ArrayNode') {
      return List(node.items.map(x => it(x)))
    }
    else if (node.type === 'AccessorNode') {
      const o = it(node.object)
      const idx = node.index.dimensions[0].value
      if (o.type !== 'list' && o.type !== 'dict') {
        throw new TypeError('cannot access elements in a ' + o.type)
      }
      if (o.type === 'list') {
        if (idx === 'x') {
          return o.value[0]
        } else if (idx === 'y') {
          return o.value[1]
        }
      }
      return o.value[idx]
    }
    else if (node.type === 'FunctionAssignmentNode') {
      const fDef = node
      funcs[fDef.name] = fDef
      return Value(0)
    }
    else if (node.type === 'ConstantNode') {
      return Value(Number(node.value))
    }
    else if (node.type === 'SymbolNode') {
      const v = getVariable(node.name)
      if (v !== undefined) {
        return v
      } else if (node.name === 'Hz') {
        return Value(1)
      } else if (node.name === 'kHz') {
        return Value(1000)
      } else if (node.name === 'deg') {
        return Value(1)
      } else if (node.name === 'rad') {
        return Value(180 / Math.PI) // 360 ÷ 2π
      } else if (node.name === 'e') {
        return Value(Math.E)
      } else if (node.name === 'pi' || node.name === 'π') {
        return Value(Math.PI)
      } else if (node.name === 'f') {
        return Value(app.baseFreq)
      } else if (node.name === 't') {
        return atg(ramp(Value(1 / scope.duration)))
        // return a ramp signal that goes from 0 to 1 in time `duration`
        /* const lfo = new Tone.LFO({
          frequency: 0.25/scope.duration,
          min: -2,
          max: 2,
          phase: 0,
          type: 'sawtooth'
        })
        return Signal(lfo) */
        /* const f = 0.25 / scope.duration
        const o = osc('saw', Value(f), Value(0))
        o.value.volume.value = 20 * Math.log10(8)
        return o */
      } else if (node.name === '_t') {
        return ramp(Value(1 / scope.duration))
        // return a ramp signal that goes from -1 to 1 in time `duration`
        /* const f = 0.5 / scope.duration
        const o = osc('saw', Value(f), Value(90))
        o.value.volume.value = 8
        return o */
      } else {
        console.warn('no such variable:', node.name)
        return Value(0)
      }
    }
    else if (node.type === 'BlockNode') {
      const res = node.blocks.map(({ node }) => it(node))
      return res[res.length - 1]
    }
    else if (node.content) {
      return it(node.content)
    } else {
      console.error('?', node)
      return Value(0)
    }
  }

  function dispose() {
    oscillators.forEach(o => o.stop())
    oscillators.forEach(o => o.dispose())

    memoIdStore.forEach(item => {
      if (!item) return
      if (item.type === 'signal') {
        item.value.stop?.() // if oscillator
        item.value.dispose()
      } else if (item.type === 'list') {
        item.value.forEach(x => {
          x.stop?.() // if oscillator
          x.dispose()
        })
      }
    })
  }

  try {
    const tree = math.parse(expr)
    const returnVal = it(tree)
    console.log({ tree })
  } catch (e) {
    errors.push(e)
  }

  const Out = (res) => {
    if (res.type === 'signal') {
      return res.value
    } else if (res.type === 'value') {
      return new Tone.Signal(res.value)
    } else {
      throw new Error('unsupported type: ' + res.type)
    }
  }

  const outX = Out(globalScope.x || Value(0))
  const outY = Out(globalScope.y || Value(0))
  // const outX = Out(globalScope.x || osc('sin', Value(app.baseFreq), Value(0)))
  // const outY = Out(globalScope.y || osc('cos', Value(app.baseFreq), Value(0)))
  const p = {
    outX,
    outY,
    globalScope,
    localScopes,
    oscillators,
    // tree,
    // returnVal,
    dispose,
    // cache,
    errors,
    memoIdStore,
  }
  console.log(p)
  return p
}

function playExpressions(expr, { out, duration, onDone, onError }) {
  try {
    const scope = { duration }
    const p = parseExpr(expr, scope)
    const { outX, outY, oscillators } = p
    if (!outX || !outY) {
      console.info(p)
    }

    const X = new Tone.Panner(-1).connect(out)
    const Y = new Tone.Panner(+1).connect(out)
    outX.connect(X)
    outY.connect(Y)
    oscillators.forEach(o => o.start())
    oscillators.forEach(o => o.stop("+" + duration))

    setTimeout(() => {
      p.dispose()
      onDone?.()
    }, 1000 * duration + 500)

    return p
  } catch (e) {
    onError?.(e)
    throw e
  }
}
async function renderExpression(expr, {
  duration = req('duration'),
  renderDuration = req('renderDuration'),
}) {
  let p
  return Tone.Offline(({ destination, transport }) => {
    p = playExpressions(expr, {
      out: destination,
      duration
    })
  }, renderDuration).then((buffer) => {
    return {
      x: buffer.getChannelData(0),
      y: buffer.getChannelData(1),
      buffer,
      p,
    }
  })
}

const om_xy_examples = [
  { title: 'Circle', expr: `x = 0.5 cos(100 Hz)\ny = 0.5 sin(100 Hz)` },
  { title: 'Square', expr: `x = 0.1 square(125 Hz)\ny = 0.1 square(125 Hz, 90)` },

  {
    group: 'Lissajous curves',
    items: [
      { title: 'Lissajous curve', expr: `f = 150 Hz\ns = 0.2\nx = s cos(f)\ny = s sin(2f)` },
      { title: 'Lissajous Bretzel / Die Breze', expr: `x = 0.2 sin(300 Hz)\ny = -0.2 cos(400 Hz)` },
      { title: 'Moving Lissajous (out-of-tune oscillators)', expr: `f = 260 Hz\ns = 0.2\nx = s cos(f)\ny = s sin(2f + 1)` },
      { title: 'A 3x3 grid of Lissajous curves', expr: `f = 450\na = [sin(f), cos(f)]\nb = [sin(3 f), -cos(4 f)]\nc = [sin(4 f), cos(2 f)]\nd = [sin(1 f), cos(3 f)]\nXY = GRID(f/10, 3, 3, 0.1, a, b, c, d, a, b, c, d, a)` },
    ],
  },
  {
    group: 'Flowers',
    items: [
      { title: 'Flower 1', expr: `a = 450\nb = a/4\nx = (sin(a) + cos(b)) / 2\ny = (cos(a) + sin(b)) / 2` },
      { title: 'Flower 2', expr: `a = 600\nb = a/12\nx = 0.9 (sin(a) + cos(b)) / 2\ny = 0.9 (cos(a) + sin(b)) / 2` },
      { title: 'Flower 3', expr: `a = 500\nb = 100\nx = 0.8 (sin(a) + sin(b)) / 2\ny = 0.8 (sin(a, +90) + sin(b, -90)) / 2` },
      { title: 'Flower 4 (dandelion)', expr: `a = 450\nb = a/5\nx = 0.7 (sin(a) + sin(b)) / 2\ny = 0.7 (cos(a) + cos(b)) / 2` },
      { title: 'Rose (rhodonea curve)', expr: `f1 = 60 Hz\nf2 = 400 Hz\nx = 0.1 cos(f2) + 0.7 cos(f1)\ny = 0.1 sin(f2) + 0.7 sin(f1)` },

      { title: 'Flower 5', expr: `K = 0.4\nf = 440\nx = 0.45 sin(f) (cos(f) - 2 cos(K * f))\ny = 0.45 cos(f) (cos(f) - 2 cos(K * f)) - 0.5` },
      { title: 'Flower 6', expr: `K = 1.5\nf = 440\nx = 0.45 sin(f) (cos(f) - 2 cos(K * f))\ny = 0.45 cos(f) (cos(f) - 2 cos(K * f)) - 0.5` },
      { title: 'Flower 7', expr: `K = 4\nf = 440\nx = 0.45 sin(f) (cos(f) - 2 cos(K * f))\ny = 0.45 cos(f) (cos(f) - 2 cos(K * f)) - 0.5` },

      { title: 'Flower 8', expr: `f = 111 Hz\ns = 0.8\nx = s * cos(f) abs(sin(3f))\ny = s * sin(f) abs(sin(3f))` },
      { title: 'A pair of flowers', expr: `Note(k) = 440 * 2^(k/12)\nx = 0.1 * (sin(Note(0)) + 2 sqr(Note(4)) + cos(Note(7), 45))\ny = 0.1 * (cos(Note(0)) + 2 sqr(Note(4)) + sin(Note(7), 45))` },
      { title: 'A grid of flowers', expr: `f = 1111 Hz\nk = 3.25111111\ns = 0.3\nx = s * cos(f) abs(sin(k*f)) + sqr(300, 90)/2\ny = s * sin(f) abs(sin(k*f)) + sqr(300)/2` },
    ]
  },
  {
    group: 'Butterflies',
    items: [
      { title: 'Simple butterfly', expr: `f = 440 Hz\nrho = EXP(cos(f)) - 2 cos(4 f)\nx = 0.25 rho sin(f)\ny = 0.25 rho cos(f)` },
      { title: 'Detailed butterfly', expr: `f = 440 Hz\nrho = EXP(cos(f)) - 2 cos(4 f) - sin(f/12)^5\nx = 0.25 rho sin(f)\ny = 0.25 rho cos(f)` },
    ]
  },
  {
    group: 'Mushrooms',
    items: [
      { title: 'Mushroom with round cap', expr: `# We create a circle\nwidth = 0.05\ncf = 789 Hz\nx = cx = width cos(cf)\ny = cy = width sin(cf)\n\n\n# We move it up across time\nheight = 0.5\nfreq = 30 Hz  # try different values: 1, 10, 30\nt = ramp(freq)\ny = cy + height * t\n\n\n# We create the cap shape by modulating the stem with a sine wave\ncapWidth = 4\nx = capX = cx * capWidth sin(freq)\n\n\n# We trim the mushroom cap at the right time\nstemWidth = 0.2\nstemTop = 0.3\nx = cx * stemWidth + (t > stemTop) * capX` },
      { title: 'Mushroom with pointy cap', expr: `f = 61 Hz\nt = ramp(f)\nfreq = 1789 Hz\nx = cx = 0.05 cos(freq)\ny = cy = 0.05 sin(freq)\ny = cy + 0.5 * t\ncapX = cx * 10 saw(f)^2\nx = iff(t > 0, capX, cx * 0.1)` },
      { title: 'A grid of mushrooms', expr: `width = 0.05\nheight = 0.3\ncap = 10\nstem = 0.1\nstemPos = 0.55\n\ngridx(X)= X + (ramp(50)>0)-0.5\ngridy(Y)= Y + (ramp(100)>0)-0.5\n\nf = 1456 Hz\ng = 10 Hz\nt = ramp(g)\nx = width cos(f) * lerp(t<stemPos, 4 sin(g), stem)\ny = width sin(f) + height * t\n\nx=gridx(x)\ny=gridy(y)` },
      { title: 'Swimming mushroom', expr: `f = 61 Hz\nt = ramp(f)\n\nfreq = 1456 Hz\nwidth = 0.05; height = 0.5\ncapWidth = 10; stemWidth = 0.1\nstemTop = 0\n\nx = cx = width cos(freq)\ny = cy = width sin(freq)\ny = cy + height * t\ncapX = cx * capWidth lerp(sin(2), sin(f/2)^2/2, saw(f)^2)\nx = iff(t > stemTop, capX, cx * stemWidth)` },
    ]
  },
  {
    group: 'Spirals',
    items: [
      { title: 'Snail', expr: `f = 110 Hz\ns = .4\nx = s * cos(f) (1+ramp(f/2))/2\ny = s * sin(f) (1+ramp(f/2))/2` },
      { title: 'Lemon', expr: `f = 111 Hz\ns = 0.2\nx = s * cos(f) * (ramp(6 f) < 0.8)\ny = s * sin(f) * (ramp(6 f) < 0.8)` },
    ],
  },
  {
    group: 'Circles',
    items: [
      { title: 'Small circle', expr: `s = 0.1\nf = 200 Hz\nx = s cos(f)\ny = s sin(f)` },
      { title: 'Growing circle', expr: `s = 0.4 sin(2)\nf = 200 Hz\nx = s cos(f)\ny = s sin(f)` },
      { title: 'Stretched circle', expr: `f = 200 Hz\nx = 0.5 cos(f)\ny = 0.3 sin(f)` },
      { title: 'Translated circle', expr: `f = 220 Hz\nx = 0.5 + 0.3 cos(f)\ny = 0.5 + 0.3 sin(f)` },
      { title: 'Duplicating a circle', expr: `f = 220 Hz\ns = 0.2\nx = s cos(f) + square(50)/3\ny = s sin(f)` },
    ],
  },
  {
    group: 'Composition techniques',
    items: [
      { title: 'Nested circles', expr: `# In this example, three pulse waves with each a duty cycle of 33.3%, and each offset by 120 degrees, are used to quickly change the value of 's', the size of the circle.\n\n# Share: (k: [0, n-1], n: integer) -> [0, 1]\nS(k,n) = 0.5 + 0.5 * pwm(330 Hz, 1/n, -(k / n) * 360)\n\nshare3(v1, v2, v3) =\n  v1 * S(0, 3) +\n  v2 * S(1, 3) +\n  v3 * S(2, 3)\n\ns = share3(0.2, 0.5, 0.8)\nx = s cos(440)\ny = s sin(440)` },
      { title: 'Basic implementation of a 2x2 grid', expr: `f = 400\n# Defining the 4 signals to display\nx1 = sin(1 f);   y1 = cos(1 f)\nx2 = sin(3 f);   y2 =-cos(4 f)\nx3 = sin(4 f);   y3 = cos(2 f)\nx4 = sin(1 f);   y4 = cos(3 f)\n# Size of a grid cell\nW(k) = 1/2\nH(k) = 1/2\n# Center of a grid cell\nX(k) = (-1/2) + k\nY(k) = (-1/2) + k\n# Moving and scaling signals\nGx(x, k) = X(k) + W(k) * x\nGy(y, k) = Y(k) + H(k) * y\n# Sequencing signals\nx = share(100, Gx(x1, 0), Gx(x2, 0), Gx(x3, 1), Gx(x4, 1))\ny = share(100, Gy(y1, 0), Gy(y2, 1), Gy(y3, 0), Gy(y4, 1))` },
      { title: 'Using the GRID predefined function', expr: `f = 450\na = [ sin(1 f),  cos(1 f) ]\nb = [ sin(3 f), -cos(4 f) ]\nc = [ sin(4 f),  cos(2 f) ]\nd = [ sin(1 f),  cos(3 f) ]\nXY = GRID(100, 2, 2, 0.1, a, b, c, d)` },
    ],
  },
  {
    group: 'Sound',
    items: [
      { title: 'Harmony', expr: `Note(k) = 440 * 2^(k/12)\nx = 0.3 * (sin(Note(5)) + sin(Note(9)))\ny = 0.3 * (sin(Note(0)) + sin(Note(12)))` },
      { title: 'Stereo tremolo', expr: `tremolo_frequency = 3 Hz\ntremolo_intensity = 1\ntremolo = (1 + sin(tremolo_frequency)) / 2\ntx = lerp(tremolo_intensity, 1, tremolo)\nty = lerp(tremolo_intensity, 1, 1 - tremolo)\nx = tx triangle(100)\ny = ty triangle(100)` },
      { title: 'Effect of GRID on sound', expr: `t = ramp01(1)\nf = 120 + (1500 t) Hz\nx = iff(t < 0.5, 2t sqr(f), (2 - 2t) cos(f))\ny = iff(t < 0.5, 2t sqr(f, 90), (2 - 2t) sin(f))\nA = [x, y]\nU = GRID(60, 2, 2, 0.01, A)\nV = GRID(60, 3, 3, 0.01, A)\nx = iff(t < 0.5, 5/6 U.x, V.x)\ny = iff(t < 0.5, 5/6 U.y, V.y)` },
    ],
  },
  {
    group: 'Miscellaneous',
    items: [
      { title: 'Noise', expr: `x = 0.3 noise()\ny = 0.3 noise()` },
      { title: 'Thick line', expr: `f = 220 Hz\nx = 0.02 cos(f) + 0.5 * ramp(1.23 f)\ny = 0.02 sin(f)` },
      { title: 'Plotting the y = x^2 equation', expr: `t = ramp(100 Hz)\nx = t\ny = t*t` },
      { title: 'Moving arrow', expr: `k = 1\nf = 100\nx = ramp(f)\ny = share(f/1, ramp(f+k), -ramp(f-k))` },
      { title: 'Shearing', expr: `f = 200 Hz\nx = 0.5 cos(f)\ny = 0.5 sin(f)\na = 0.5\nx = x\ny = a x + y` },

      { title: 'The XY special variable', expr: `XY = [cos(100), sin(100)]` },
      { title: 'Plotting a sine wave', expr: `F = 120\nx = ramp(F)\ny = 0.4 * sin(4 F)` },
      { title: 'Plotting a sine wave using the SIN macro', expr: `F = 120\nx = ramp(F)\ny = 0.4 * SIN(2x * 2pi)` },
      { title: 'Plotting sine waves', expr: `F = 440\nx = ramp(F)\ny = share(F/6,\n+2/3,\n(1/4) SIN(2x * 2 pi) + 2/3,\n0,\n(1/4) COS(2x * 2 pi),\n-2/3,\n(1/4) SIN(4x * 2 pi) - 2/3\n)` },
      { title: 'Quality difference between saw and ramp', expr: `f = 1000 Hz\nA = saw(f, 180)\nB = ramp(f)\n\nk = f / 6\nx = ramp(2k)\ny = lerp(ramp(k) > 0, 0.25 A + 0.5, 0.25 B - 0.5)` },
    ],
  },
  {
    group: 'Math',
    items: [
      { title: 'Basic math', expr: `x = 1 + 2\ny = 1 - 2\nz = 1 * 2\nA = 1 / 2\nB = 1 % 2` },
      { title: 'Basic trigonometry', expr: `x = 1 + 2 * COS(1 rad)\ny = 1 + 2 * SIN(1 rad)\nz = 1 + 2 * TAN(1 rad)` },
    ],
  },
  {
    group: 'Shroom step-by-step guide',
    items: [
      ...(function* () {
        const step1 = `# 1\ufe0f\u20e3 We create a circle \u25ef\ncircle_freq = 880 Hz\nx = cos(circle_freq)\ny = sin(circle_freq)`
        const step2 = `# 1\ufe0f\u20e3 We create a small circle \u25cb\ncircle_freq = 880 Hz\nwidth = 0.02\nx = cx = width * cos(circle_freq)\ny = cy = width * sin(circle_freq)`
        const step3 = `${step2}\n\n# 2\ufe0f\u20e3 We move the circle up across time to make the stem appear\nstem_freq = 30 Hz\nheight = 0.5\ny = cy + height * ramp(stem_freq)`
        const step4 = `${step3}\n\n# 3\ufe0f\u20e3 We create the mushroom cap shape by modulating the stem with a sine wave\ncap_width = 10\nx = cap_x = cx * cap_width * sin(stem_freq)`
        const step5 = `${step4}\n\n# 4\ufe0f\u20e3 We cut the modulation to make the cap appear\ncap_pos = 0.15\ncut = (y > cap_pos)\nx = cx + cut * cap_x`
        const step6 = `${step3}\n\n# 3\ufe0f\u20e3 We create the mushroom cap shape by modulating the stem with a sine wave\ncap_width = 20  # 10 → 20\nx = cap_x = cx * cap_width * saw(stem_freq)^2  # sin → saw^2\n\n# 4\ufe0f\u20e3 We cut the modulation to make the cap appear\ncap_pos = 0.05  # 0.15 → 0.05\ncut = (y > cap_pos)\nx = cx + cut * cap_x`
        const step7 = step6.replace(/^cut =.*$/, 'cut = atg(-alignedPulse(freq, stemTop))  # using a pulse wave to cut the modulation')
        yield { title: '1. We start with a circle', expr: step1 }
        yield { title: '2. We shrink it down', expr: step2 }
        yield { title: '3. We move it up to create the stem', expr: step3 }
        yield { title: '4. We create the cap by modulating with a sine', expr: step4 }
        yield { title: '5. We cut the modulation to make the cap appear', expr: step5 }
        yield { title: '6. We can even change the shape of the cap', expr: step6 }
        yield { title: '7. Extra. Using alignedPulse for the cut', expr: step7 }
      })(),
    ],
  }
]

function initCust() {
  const customPlayButton = document.getElementById('om-xy-play')
  const previewCanvas = document.getElementById('om-xy-white-canvas')
  const nextExampleBtn = document.getElementById('om-xy-example-next-button')
  const examplesSelect = document.getElementById('om-xy-example-dropdown')
  let renderCanvas = document.getElementById('om-xy-black-canvas')

  let idx = 0
  const ctx = previewCanvas.getContext('2d')
  let duration = 1

  const _drawPreview = async () => {
    idx += 1
    const myIdx = idx

    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height)

    const renderDuration = duration
    const xy = await renderExpression(exprInput.value, { duration: duration, renderDuration })

    const errors = xy.p.errors
    if (errors && errors.length > 0) {
      xy.p.dispose()
      for (const err of errors) {
        console.error(err)
      }
      return
    }

    const bufferLength = Math.min(xy.x.length, xy.y.length)
    const fullLength = Math.floor((duration / renderDuration) * bufferLength)

    const draw = (o, d, incr = 1) => {
      drawRawOscilloscope(xy, ctx, {
        offset: o,
        length: d,
        increment: incr || 1,
        fullLength,
      })
    }
    let startTime = -1
    let prevTime = -1
    let stop = false
    const frame = (frameTime) => {
      if (idx !== myIdx) { return }
      if (stop) { return }

      if (startTime < 0) {
        startTime = frameTime
        prevTime = frameTime
      }

      const t = frameTime - startTime

      if (t >= 1000 * duration) {
        startTime = frameTime
        prevTime = frameTime
        stop = true
        // draw(0, fullLength)
        return
      }

      let d = 1000
      const m = fullLength - d - 1
      const pct = t / (1000 * duration)
      const o = Math.floor(m * pct)

      const incr = (frameTime - prevTime > 1000/45) ? 2 : 1
      draw(o, d, incr)
      prevTime = frameTime
      requestAnimationFrame(frame)
    }
    requestAnimationFrame(frame)
  }
  const drawPreview = () => {
    return _drawPreview().catch(console.warn)
  }

  const exprInput = document.getElementById('om-xy-input')
  const getUrlEncodedExpr = () => {
    return '#xy=' + encodeURIComponent(exprInput.value.trim())
  }
  const onHashChange = () => {
    const hash = document.location.hash
    const regex = /^#xy=/g
    if (hash !== getUrlEncodedExpr()) {
      if (hash.match(regex)) {
        exprInput.value = decodeURIComponent(hash.replace(regex, ''))
        setTimeout(() => {
          document.getElementById('om-xy-playground')?.scrollIntoView({ behavior: 'auto', block: 'start' })
            drawPreview()
        }, 100)
      }
    }
  }
  window.addEventListener('hashchange', onHashChange)
  onHashChange()

  exprInput.value = exprInput.value
    .split(/\r?\n/gmui)
    .map(s => s.trim())
    .join('\n')

  const previousSources = []
  let vIdx = 0

  const redrawBoth = async (opts = {}) => {
    const { playSound = false } = opts
    await drawPreview()
    await drawWoscopeAndPlay({ playSound })
  }

  const durationSlider = document.getElementById('om-render-duration')
  durationSlider?.addEventListener('change', async () => {
    duration = Number(durationSlider.value)
    await redrawBoth()
  })

  const drawWoscopeAndPlay = async (opts = {}) => {
    const { playSound = true } = opts
    const myVIdx = (++vIdx)
    previousSources.forEach(p => {
      if (p.state !== 'stopped') { p.stop() }
      p.dispose()
    })
    previousSources.length = 0 // emptying the array

    const xy = await renderExpression(exprInput.value, { duration, renderDuration: duration })
    xy.p.dispose()

    const source = new Tone.BufferSource({
      url: xy.buffer,
      loop: true,
      fadeIn: 0.01,
      fadeOut: 0.01,
    })
    previousSources.push(source)
    const node = source._source._nativeAudioBufferSourceNode

    if (playSound) {
      const g = new Tone.Gain(0.5)
      g.connect(app.main)
      source.connect(g)
    }

    function onDone() {
      if (myVIdx !== vIdx) {
        return // a more recent viz exists
      }

      renderCanvas = app.destroyWoscope(renderCanvas)

      if (source.state !== 'stopped') { source.stop() }
      source.dispose()

      previousSources.forEach(p => {
        if (p.state !== 'stopped') { p.stop() }
        p.dispose()
      })
      previousSources.length
    }

    renderCanvas = app.initWoscope(renderCanvas, node)
    source.onended = onDone
    const now = Tone.now()
    source.start(now)
    source.stop(now + duration - source.fadeOut)
  }

  const onInput = async () => {
    console.log('latex:', exprInput.value.replace(/\r?\n/g,'\\%0A').replace(/#/g,'\\%23'))
    console.log('js:', '{ title: \'\', expr: `' + exprInput.value.replace(/\r?\n/g,'\\n').replace(/`/g,'\\`') + "` },")
    window.history.replaceState(null, null, getUrlEncodedExpr())
    drawPreview()
  }

  exprInput.addEventListener('input', () => {
    onInput()
  })
  customPlayButton.addEventListener('click', async () => {
    await app.start()
    await drawWoscopeAndPlay({ playSound: true })
  })

  async function updateFromSelectedExample() {
    const expr = examplesSelect.value
    const regex = /^#xy=/g
    if (expr.match(regex)) {
      // url encoded example
      exprInput.value = decodeURIComponent(expr.replace(regex, ''))
    } else {
      exprInput.value = expr
    }
    window.history.replaceState(null, null, getUrlEncodedExpr())
    await drawPreview()
    await drawWoscopeAndPlay({ playSound: true })
  }
  examplesSelect.addEventListener('change', () => {
    updateFromSelectedExample()
  })
  nextExampleBtn.addEventListener('click', async () => {
    const idx = examplesSelect.selectedIndex
    const n = examplesSelect.options.length
    examplesSelect.selectedIndex = (idx + 1) % n

    await updateFromSelectedExample()
  })

  function populateExamples(examples, container) {
    for (const ex of examples) {
      if (ex.group) {
        const { group, items } = ex
        const optGroup = document.createElement('optgroup')
        optGroup.label = group
        container.appendChild(optGroup)
        populateExamples(items, optGroup)
      } else {
        const { title, expr } = ex
        const opt = document.createElement('option')
        opt.text = title
        opt.value = expr
        container.appendChild(opt)
      }
    }
  }

  const emptyExample = document.createElement('option')
  emptyExample.text = ''
  emptyExample.value = ''
  examplesSelect.appendChild(emptyExample)
  populateExamples(om_xy_examples, examplesSelect)

  emptyExample.selected = true
  for (const opt of examplesSelect.options) {
    if (opt.value.trim() === exprInput.value.trim()) {
      opt.selected = true
      break
    }
  }

  drawPreview()
}


for (const el of document.querySelectorAll('apply-template')) {
  const id = el.getAttribute('template')
  const slots = Object.fromEntries(
    el.getAttributeNames()
      .map(k => [k, el.getAttribute(k)])
  )
  el.replaceWith(getTemplate(id, slots))
}

app.waitForStart(() => {
  const freqMultSlider = document.getElementById('om-freqMult-slider')
  if (freqMultSlider) {
    const update = () => app.freqMult = Number(freqMultSlider.value)
    freqMultSlider.addEventListener('change', () => {
      const prevMult = Number(app.freqMult)
      const nextMult = Number(freqMultSlider.value)

      const sliders = document.querySelectorAll(`input[slot-key="freq"][min][max]`)
      sliders.forEach(s => {
        s.min = Math.round(s.min * nextMult / prevMult)
        s.max = Math.round(s.max * nextMult / prevMult)
      })
      update()
      renderAll()
    })
    update()
  }

  const volumeSlider = document.getElementById('om-volume-slider')
  if (volumeSlider) {
    const update = () => app.master.gain.value = 0.6 * Number(volumeSlider.value)
    volumeSlider.addEventListener('change', () => {
      update()
      renderAll()
    })
    update()
  }
  renderAll()
})

window.addEventListener('click', () => app.start(), { once: true })
window.addEventListener('keypress', () => app.start(), { once: true })
window.addEventListener('resize', () => renderAll())

renderAll()
initCust()
